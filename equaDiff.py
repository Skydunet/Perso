import numpy as np
from scipy import integrate
from matplotlib import pyplot as plt

#----Equation Simple----
def equaD(yPrim, a, b, n, ya):#prim de y'(x, y) entre a et b avec n points tel que y(a)=ya
    dx = (b-a)/(n-1)
    tabx = np.linspace(a, b, n)
    taby = [0] * n
    taby[0] = ya
    for i in range(1, n):
        taby[i] = taby[i-1] + dx*yPrim(tabx[i], taby[i])
    return tabx, taby

res = equaD(lambda x, y:x - 2*y, 0, 20, 200, 0)#on resout y' +2y = x <=> y' = x -2y

#----Résolution de n Equations liées----

def equaDB(ysPrims, a, b, n, yas):#prim des y'(x, y) entre a et b avec n points tel que y[i](a)=ya[i]
    dx = (b-a)/(n-1)
    x = np.linspace(a, b, n)
    y = np.array([[0 for i in range(len(yas))] for i in range(n)])
    for i in range(len(y)):
        y[i][0] = yas[i]
    for i in range(1, n):
        for ii in range(len(y[i])):
            y[i][ii] = y[i-1][ii] + dx*ysPrims(x[i][ii], y[i][ii])
    return x, y

w0 = 1
Q = 42

def eq(y, t):
    return [y[1], -(w0/Q)*y[1] -(w0**2)*y[0]]

t = np.linspace(0, 9, 100)
res = integrate.odeint(eq, [3.1, 0], t)
y = res[:,0]
y1 = res[:,1]

plt.plot(y, y1, "-", t, y, "-")
plt.show()

def exNA():
    x, y = equaD((lambda x, y:(2**(-x**2))), -5, 5, 500, -1)
    plt.plot(x, y)
    plt.show()

exNA()

def ex9(n):
    f = lambda x:2**(-x**2)
    dx = 10/(n-1)
    xs = np.linspace(-5, 5, n)
    s = 0
    for x in xs:
        s += f(x)*dx
    return s

print(ex9(200))

def ex11(n):
    f = lambda x:2**(-x**2)
    dx = 10/(n-1)
    x = np.linspace(-5, 5, n)
    s = 0
    for i in range(n-1):
        s += dx*(f(x[i]) + ((f(x[i+1])-f(x[i]))/2))
    return s

print(ex11(200))

print(integrate.quad(lambda x:2**(-x**2), -5, 5))