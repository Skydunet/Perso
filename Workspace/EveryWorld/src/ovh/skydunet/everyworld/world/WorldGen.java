package ovh.skydunet.everyworld.world;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

public class WorldGen extends ChunkGenerator {
	public WorldGen(){
		
	}
	public Location getFixedSpawnLocation(World world, Random random){
		return new Location(world, 20, 20, 20);
	}
	
	public List<BlockPopulator> getDefaultPopulators(World world) {
		return new ArrayList<BlockPopulator>();
	}
	
	@Override
	public short[][] generateExtBlockSections(World world, Random random, int chunkX, int chunkZ, BiomeGrid biomes){
		short[][] result = new short[256 / 16][];
		int x, y, z;

		for (x = 0; x < 16; x++){
			for (z = 0; z < 16; z++){
				for (y = 0; y <= 0; y++){
					setBlock(result, x, y, z, (short)4);
				}
			}
		}
		return result;
	}
	
	private void setBlock(short[][] result, int x, int y, int z, short blockId) {
		if (result[y >> 4] == null){
			result[y >> 4] = new short[4096];
		}
		result[y >> 4][((y & 0xF) << 8) | (z << 4) | x] = blockId;
	}
}
