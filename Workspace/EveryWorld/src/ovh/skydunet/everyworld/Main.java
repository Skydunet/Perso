package ovh.skydunet.everyworld;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldType;
import org.bukkit.World.Environment;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import ovh.skydunet.everyworld.world.*;

public class Main extends JavaPlugin implements Listener{
	
	private FileConfiguration config, configB;
	private List<String> protalsConfig = new ArrayList<String>();
	private List<Portal> portals = new ArrayList<Portal>();
	private List<String> worlds = new ArrayList<String>();
	
	@Override
	public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getLogger().info("Everyworld enabled");
        File fichierConfig = new File("plugins/EveryWorld/data.yml");
        config = YamlConfiguration.loadConfiguration(fichierConfig);
        File fichierConfigB = new File("plugins/EveryWorld/config.yml");
        configB = YamlConfiguration.loadConfiguration(fichierConfigB);
        if (config.isSet("portals")){
        	protalsConfig = config.getStringList("portals");
        }
        if (config.isSet("worlds")){
        	worlds = config.getStringList("worlds");
        }
        if (!configB.isSet("ResourcePacks")){
        	configB.set("ResourcePacks", false);
        }
        if (!configB.isSet("WorldGamemode")){
        	configB.set("WorldGamemode", false);
        }
        if (!configB.isSet("ChangeOpGamemode")){
        	configB.set("ChangeOpGamemode", false);
        }
        if (!configB.isSet("WorldResources")){
        	configB.set("WorldResources.world", "https://");
        	configB.set("WorldResources.world_nether", "https://");
        }
        try {
			configB.save(new File("plugins/EveryWorld/config.yml"));
		} catch (IOException e) {
			Bukkit.getConsoleSender().sendMessage(e.getMessage());
		}
        for (String s : protalsConfig){
        	String[] t = s.split(",");
        	Location loc = new Location(Bukkit.getWorld(t[3]), (double)Integer.parseInt(t[0]), (double)Integer.parseInt(t[1]), (double)Integer.parseInt(t[2]));
        	Location dest = new Location(Bukkit.getWorld(t[7]), (double)Integer.parseInt(t[4]), (double)Integer.parseInt(t[5]), (double)Integer.parseInt(t[6]));
        	portals.add(new Portal(dest, loc));
        }
        for (String w : worlds){
        	String[] m = w.split(",");
        	MainGenerator mainGenerator = new MainGenerator(m[0]);
			WorldType type = null;
			Environment env = null;
			if (m[1].equalsIgnoreCase("flat")){
				env = Environment.NORMAL;
				type = WorldType.FLAT;
			}else if (m[1].equalsIgnoreCase("normal")){
				type = WorldType.NORMAL;
				env = Environment.NORMAL;
			}else if (m[1].equalsIgnoreCase("amplified")){
				type = WorldType.AMPLIFIED;
				env = Environment.NORMAL;
			}else if (m[1].equalsIgnoreCase("large")){
				type = WorldType.LARGE_BIOMES;
				env = Environment.NORMAL;
			}else if (m[1].equalsIgnoreCase("normal")){
				type = WorldType.NORMAL;
				env = Environment.NORMAL;
			}else if (m[1].equalsIgnoreCase("nether")){
				type = WorldType.NORMAL;
				env = Environment.NETHER;
			}else if (m[1].equalsIgnoreCase("end")){
				type = WorldType.NORMAL;
				env = Environment.THE_END;
			}else if (m[1].equalsIgnoreCase("cobble")){
				type = WorldType.NORMAL;
				env = Environment.NORMAL;
				mainGenerator.setGenerator("s");
			}else if (m[1].equalsIgnoreCase("air")){
				type = WorldType.NORMAL;
				env = Environment.NORMAL;
				mainGenerator.setGenerator("a");
			}else if (m[1].equalsIgnoreCase("random")){
				type = WorldType.NORMAL;
				env = Environment.NORMAL;
				mainGenerator.setGenerator("rf");
			}else if (m[1].equalsIgnoreCase("space")){
				type = WorldType.NORMAL;
				env = Environment.NORMAL;
				mainGenerator.setGenerator("sp");
			}
			if (m[4].equalsIgnoreCase("true")){
				mainGenerator.setType(type);
				mainGenerator.setEnv(env);
				if (m[2] == "true"){
					mainGenerator.setStructureState(true);
				}else{
					mainGenerator.setStructureState(false);
				}
				mainGenerator.generate(Bukkit.getConsoleSender());
			}
        }
    }
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("world")){//faire la commande de help
			if (args.length >= 1){
				if (args[0].equalsIgnoreCase("help")){
					sender.sendMessage(ChatColor.RED + "/world <create:list:move:portal:remove:unload:load:gamemode:autoload>");
					return true;
				}
				if (args.length != 3 && args[0].equalsIgnoreCase("gamemode")){
					sender.sendMessage(ChatColor.RED + "/world gamemode <WorldName> <Gamemode>");
					return true;
				}
				if (args.length == 3 && args[0].equalsIgnoreCase("gamemode")){
					int ind = -2;
					int i = 0;
					for (String w : worlds){
						if (w.split(",")[0].equalsIgnoreCase(args[1])){
							ind = i;
						}
						i++;
					}
					if (ind != -2){
						if (args[2].equalsIgnoreCase("0") || args[2].equalsIgnoreCase("1") || args[2].equalsIgnoreCase("2") || args[2].equalsIgnoreCase("3") || args[2].equalsIgnoreCase("remove")){
							String str = "";
							if (args[2].equalsIgnoreCase("remove")){
								str = worlds.get(ind).split(",")[0] + "," + worlds.get(ind).split(",")[1] + "," + worlds.get(ind).split(",")[2] + ",-2," + worlds.get(ind).split(",")[4];
							}else{
								str = worlds.get(ind).split(",")[0] + "," + worlds.get(ind).split(",")[1] + "," + worlds.get(ind).split(",")[2] + "," + args[2] + "," + worlds.get(ind).split(",")[4];
							}
							worlds.set(ind, str);
							sender.sendMessage(ChatColor.AQUA + "World gamemode edited !");
						}else{
							sender.sendMessage(ChatColor.RED + "Wrong Gamemode !");
						}
					}else{
						sender.sendMessage(ChatColor.RED + "This world does not exists !");
					}
					return true;
				}
				if (args.length != 1 && args[0].equalsIgnoreCase("list")){
					sender.sendMessage(ChatColor.RED + "/world list");
					return true;
				}
				if (args.length == 1 && args[0].equalsIgnoreCase("list")){
					sender.sendMessage(ChatColor.AQUA + "World List :");
					for (String w : worlds){
						String autoLoad = "";
						if (w.split(",")[4].equalsIgnoreCase("true")){
							autoLoad = ChatColor.GREEN + "■" + ChatColor.AQUA;
						}else{
							autoLoad = ChatColor.RED + "■" + ChatColor.AQUA;
						}
						String status = ChatColor.RED + "■" + ChatColor.AQUA;
						for (World ww : Bukkit.getWorlds()){
							if (ww.getName().equalsIgnoreCase(w.split(",")[0])){
								status = ChatColor.GREEN + "■" + ChatColor.AQUA;
							}
						}
						if (w.split(",")[3].equalsIgnoreCase("-2")){
							sender.sendMessage(ChatColor.AQUA + "-Name : " + w.split(",")[0] + " | Type : " + w.split(",")[1] + " | Autoload : " + autoLoad + " | Status : " + status);
						}else{
							sender.sendMessage(ChatColor.AQUA + "-Name : " + w.split(",")[0] + " | Type : " + w.split(",")[1] + " | Gamemode : " + w.split(",")[3] + " | Autoload : " + autoLoad + " | Status : " + status);
						}
					}
					return true;
				}
				if (args.length != 2 && args[0].equalsIgnoreCase("remove")){
					sender.sendMessage(ChatColor.RED + "/world remove <WorldName>");
					return true;
				}
				if (args.length == 2 && args[0].equalsIgnoreCase("remove")){
					int ind = -2;
					int i = 0;
					for (String w : worlds){
						if (w.split(",")[0].equalsIgnoreCase(args[1])){
							ind = i;
						}
						i++;
					}
					if (ind != -2){
						worlds.remove(ind);
						Bukkit.getServer().unloadWorld(worlds.get(ind).split(",")[0], true);
						sender.sendMessage(ChatColor.AQUA + "World removed ! You can get back the world by generating a world with the same name and pressets !");
					}else{
						sender.sendMessage(ChatColor.RED + "This world does not exists !");
					}
					return true;
				}
				if (args.length != 1 && args[0].equalsIgnoreCase("list")){
					sender.sendMessage(ChatColor.RED + "/world list");
					return true;
				}
				if (args.length == 1 && args[0].equalsIgnoreCase("list")){
					sender.sendMessage(ChatColor.AQUA + "World List :");
					for (String w : worlds){
						if (w.split(",")[3].equalsIgnoreCase("-2")){
							sender.sendMessage(ChatColor.AQUA + "-Name : " + w.split(",")[0] + " | Type : " + w.split(",")[1]);
						}else{
							sender.sendMessage(ChatColor.AQUA + "-Name : " + w.split(",")[0] + " | Type : " + w.split(",")[1] + " | Gamemode : " + w.split(",")[3]);
						}
					}
					return true;
				}
				if (args.length != 3 && args[0].equalsIgnoreCase("autoload")){
					sender.sendMessage(ChatColor.RED + "/world autoload <WorldName> <true:false>");
					return true;
				}
				if (args.length == 3 && args[0].equalsIgnoreCase("autoload")){
					int ind = -2;
					int i = 0;
					for (String w : worlds){
						if (w.split(",")[0].equalsIgnoreCase(args[1])){
							ind = i;
						}
						i++;
					}
					if (ind != -2){
						if (args[2].equalsIgnoreCase("true") || args[2].equalsIgnoreCase("false")){
							String str = "";
							str = worlds.get(ind).split(",")[0] + "," + worlds.get(ind).split(",")[1] + "," + worlds.get(ind).split(",")[2] + "," + worlds.get(ind).split(",")[3] + "," + args[2];
							worlds.set(ind, str);
							sender.sendMessage(ChatColor.AQUA + "Load process of this world changed !");
						}else{
							sender.sendMessage(ChatColor.RED + "Wrong arguments !");
						}
					}else{
						sender.sendMessage(ChatColor.RED + "This world does not exists !");
					}
					return true;
				}
				if (args.length != 2 && args[0].equalsIgnoreCase("unload")){
					sender.sendMessage(ChatColor.RED + "/world unload <WorldName>");
					return true;
				}
				if (args.length == 2 && args[0].equalsIgnoreCase("unload")){
					int ind = -2;
					int i = 0;
					for (String w : worlds){
						if (w.split(",")[0].equalsIgnoreCase(args[1])){
							ind = i;
						}
						i++;
					}
					if (ind != -2){
						sender.sendMessage(ChatColor.AQUA + "World unloaded !");
						Bukkit.getServer().unloadWorld(worlds.get(ind).split(",")[0], true);
					}else{
						sender.sendMessage(ChatColor.RED + "This world does not exists !");
					}
					return true;
				}
				if (args.length != 2 && args[0].equalsIgnoreCase("load")){
					sender.sendMessage(ChatColor.RED + "/world load <WorldName>");
					return true;
				}
				if (args.length == 2 && args[0].equalsIgnoreCase("load")){
					int ind = -2;
					int i = 0;
					for (String w : worlds){
						if (w.split(",")[0].equalsIgnoreCase(args[1])){
							ind = i;
						}
						i++;
					}
					if (ind != -2){
						sender.sendMessage(ChatColor.AQUA + "World loaded !");
						String[] m = worlds.get(ind).split(",");
			        	MainGenerator mainGenerator = new MainGenerator(m[0]);
						WorldType type = null;
						Environment env = null;
						if (m[1].equalsIgnoreCase("flat")){
							env = Environment.NORMAL;
							type = WorldType.FLAT;
						}else if (m[1].equalsIgnoreCase("normal")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
						}else if (m[1].equalsIgnoreCase("amplified")){
							type = WorldType.AMPLIFIED;
							env = Environment.NORMAL;
						}else if (m[1].equalsIgnoreCase("large")){
							type = WorldType.LARGE_BIOMES;
							env = Environment.NORMAL;
						}else if (m[1].equalsIgnoreCase("normal")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
						}else if (m[1].equalsIgnoreCase("nether")){
							type = WorldType.NORMAL;
							env = Environment.NETHER;
						}else if (m[1].equalsIgnoreCase("end")){
							type = WorldType.NORMAL;
							env = Environment.THE_END;
						}else if (m[1].equalsIgnoreCase("cobble")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("s");
						}else if (m[1].equalsIgnoreCase("air")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("a");
						}else if (m[1].equalsIgnoreCase("random")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("rf");
						}else if (m[1].equalsIgnoreCase("space")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("sp");
						}
						mainGenerator.setType(type);
						mainGenerator.setEnv(env);
						if (m[2] == "true"){
							mainGenerator.setStructureState(true);
						}else{
							mainGenerator.setStructureState(false);
			        	}
						mainGenerator.generate(sender);
					}else{
						sender.sendMessage(ChatColor.RED + "This world does not exists !");
					}
					return true;
				}
				if (args[0].equalsIgnoreCase("create")){
					if (args.length == 4 || args.length == 3){
						MainGenerator mainGenerator = new MainGenerator(args[1]);
						WorldType type;
						Environment env;
						if (args[2].equalsIgnoreCase("flat")){
							env = Environment.NORMAL;
							type = WorldType.FLAT;
						}else if (args[2].equalsIgnoreCase("normal")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("amplified")){
							type = WorldType.AMPLIFIED;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("large")){
							type = WorldType.LARGE_BIOMES;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("normal")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("nether")){
							type = WorldType.NORMAL;
							env = Environment.NETHER;
						}else if (args[2].equalsIgnoreCase("end")){
							type = WorldType.NORMAL;
							env = Environment.THE_END;
						}else if (args[2].equalsIgnoreCase("cobble")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("s");
						}else if (args[2].equalsIgnoreCase("air")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("a");
						}else if (args[2].equalsIgnoreCase("random")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("rf");
						}else if (args[2].equalsIgnoreCase("space")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("sp");
						}else{
							sender.sendMessage(ChatColor.RED + "This presset does not exeists");
							return true;
						}
						mainGenerator.setType(type);
						mainGenerator.setEnv(env);
						boolean structure;
						if (args.length == 4){
							if (args[3].equalsIgnoreCase("true")){
								structure = true;
							}else{
								structure = false;
							}
							worlds.add(args[1] + "," + args[2] + "," + args[3] + ",-2,true");
						}else{
							structure = true;
							worlds.add(args[1] + "," + args[2] + ",true,-2,true");
						}
						mainGenerator.setStructureState(structure);
						mainGenerator.generate(sender);
						//Bukkit.createWorld(new WorldCreator(args[2]));
					}else{
						sender.sendMessage(ChatColor.RED + "/world create <WorldName> <WorldGeneration> [generateStructures]");
					}
					return true;
				}else if (args[0].equalsIgnoreCase("move")){
					MovePlayer mover = new MovePlayer();
					Player player = null;
					if (args.length >= 2){
						for (Player p : Bukkit.getOnlinePlayers()){
							if (p.getName().equalsIgnoreCase(args[1])){
								player = p;
							}
						}
					}else{
						sender.sendMessage(ChatColor.RED + "You need to specify a player");
						return true;
					}
					if (args.length == 3){
						if (player != null){
							boolean found = false;
							List<World> worlds = Bukkit.getWorlds();
							for (World world : worlds){
								if (world.getName().equalsIgnoreCase(args[2])){
									found = true;
								}
							}
							if (found){
								mover.moveToSpawn(player, args[2]);
								sender.sendMessage(ChatColor.AQUA + player.getName() + " is moved to the world " + args[2] + " !");
								return true;
							}else{
								sender.sendMessage(ChatColor.RED + "Unable to find the world " + args[2]);
								return true;
							}
						}else{
							sender.sendMessage(ChatColor.RED + "Unable to find the player " + args[1]);
							return true;
						}
					}else if (args.length == 6){
						if (player != null){
							boolean found = false;
							List<World> worlds = Bukkit.getWorlds();
							for (World world : worlds){
								if (world.getName().equalsIgnoreCase(args[2])){
									found = true;
								}
							}
							if (found){
								mover.moveToCoords(player, args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]));
								sender.sendMessage(ChatColor.AQUA + player.getName() + " is moved to the world " + args[2] + " !");
								return true;
							}else{
								sender.sendMessage(ChatColor.RED + "Unable to find the world " + args[2]);
								return true;
							}
						}else{
							sender.sendMessage(ChatColor.RED + "Unable to find the player " + args[1]);
							return true;
						}
					}else{
						sender.sendMessage(ChatColor.RED + "You need to precize World & Player that you want to move");
						return true;
					}
				}else if (args[0].equalsIgnoreCase("portal")){
					if (args.length == 5){
						String name = sender.getName();
						Player pl = null;
						for (Player p : Bukkit.getOnlinePlayers()){
							if (p.getName() == name){
								pl = p;
							}
						}
						World destWorld = null;
						for (World w : Bukkit.getWorlds()){
							if (w.getName().equalsIgnoreCase(args[1])){
								destWorld = w;
							}
						}
						if (destWorld != null){
							Location portalLoc = pl.getTargetBlock((Set<Material>)null, 20).getLocation();
							Portal por = new Portal(new Location(destWorld, (double)Integer.parseInt(args[2]), (double)Integer.parseInt(args[3]), (double)Integer.parseInt(args[4])), portalLoc);
							portals.add(por);
							sender.sendMessage(ChatColor.AQUA + "Portal Created !");
						}else{
							sender.sendMessage(ChatColor.RED + "This world does not exists !");
						}
					}else{
						sender.sendMessage(ChatColor.RED + "/world portal <WorldName> <x> <y> <z>");
					}
					return true;
				}
			}
		}else if (cmd.getName().equalsIgnoreCase("goto")){
			if (args.length == 1 && !args[0].equalsIgnoreCase("help")){
				World destWorld = null;
				for (World w : Bukkit.getWorlds()){
					if (w.getName().equalsIgnoreCase(args[0])){
						destWorld = w;
					}
				}
				if (destWorld != null){
					MovePlayer mover = new MovePlayer();
					mover.moveToSpawn(Bukkit.getPlayer(sender.getName()), args[0]);
					sender.sendMessage(ChatColor.AQUA + "Teleported to " + args[0]);
				}else{
					sender.sendMessage(ChatColor.RED + "This world does not exists !");
				}
			}else{
				sender.sendMessage(ChatColor.RED + "/goto <WorldName>");	
			}
			return true;
		}
		return false;
	}
	
	public void update(){
		Bukkit.getScheduler().runTaskTimer(this, new Runnable(){
			@Override
			public void run() {
				
			}
		}, 0, 20);
	}
	
	@EventHandler
	public void playerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		Location loc = p.getLocation();
		for (Portal po : portals){
			if (po.loc.getWorld() == loc.getWorld() && po.loc.getBlockX() == loc.getBlockX() && po.loc.getBlockY() == loc.getBlockY() && po.loc.getBlockZ() == loc.getBlockZ()){
				MovePlayer mover = new MovePlayer();
				mover.moveToCoords(p, po.destination.getWorld().getName(), (int)po.destination.getX(), (int)po.destination.getY(), (int)po.destination.getZ());
			}
		}
	}
	
	@Override
    public void onDisable(){//save toutes les données enregistrées dans les variables genre les portals
		List<String> lst = new ArrayList<String>();
		for (Portal p : portals){
			String[] t = new String[8];
			t[0] = p.loc.getBlockX() + "";
			t[1] = p.loc.getBlockY() + "";
			t[2] = p.loc.getBlockZ() + "";
			t[3] = p.loc.getWorld().getName();
			t[4] = p.destination.getBlockX() + "";
			t[5] = p.destination.getBlockY() + "";
			t[6] = p.destination.getBlockZ() + "";
			t[7] = p.destination.getWorld().getName();
			lst.add(t[0] + "," + t[1] + "," + t[2] + "," + t[3] + "," + t[4] + "," + t[5] + "," + t[6] + "," + t[7]);
		}
		config.set("portals", lst);
		config.set("worlds", worlds);
		try {
			config.save(new File("plugins/EveryWorld/data.yml"));
		} catch (IOException e) {
			Bukkit.getConsoleSender().sendMessage(e.getMessage());
		}
    }
	
	@EventHandler
	public void onPlayerChangeWorld(PlayerChangedWorldEvent e){
		Player p = e.getPlayer();
		World w = p.getWorld();
		if (configB.isSet("WorldResources." + w.getName()) && configB.isSet("ResourcePacks") && configB.getBoolean("ResourcePacks")){
			String resourceUrl = configB.getString("WorldResources." + w.getName());
			if (resourceUrl != null){
				p.setResourcePack(resourceUrl);
			}
		}
		if (configB.isSet("WorldGamemode") && configB.getBoolean("WorldGamemode") && config.isSet("worlds")){
			for (String ww : worlds){
				String[] wData = ww.split(",");
				if (wData[0].equalsIgnoreCase(w.getName()) && !wData[3].equalsIgnoreCase("-2")){
					if (!p.isOp() || (configB.isSet("ChangeOpGamemode") && configB.getBoolean("ChangeOpGamemode"))){
						if (wData[3].equalsIgnoreCase("0")){
							p.setGameMode(GameMode.SURVIVAL);
						}
						if (wData[3].equalsIgnoreCase("1")){
							p.setGameMode(GameMode.CREATIVE);
						}
						if (wData[3].equalsIgnoreCase("2")){
							p.setGameMode(GameMode.ADVENTURE);
						}
						if (wData[3].equalsIgnoreCase("3")){
							p.setGameMode(GameMode.SPECTATOR);
						}
					}
				}
			}
		}
	}
	
	/*@EventHandler
	public void onResourcePackStatus(PlayerResourcePackStatusEvent e){
		if (!e.getPlayer().isOp()){
			if (e.getStatus() == Status.DECLINED || e.getStatus() == Status.FAILED_DOWNLOAD){
				e.getPlayer().kickPlayer(ChatColor.RED + "You need to accept Ressource Pack");
			}
		}
	}*/
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		World w = p.getWorld();
		if (configB.isSet("WorldResources." + w.getName()) && configB.isSet("ResourcePacks") && configB.getBoolean("ResourcePacks")){
			String resourceUrl = configB.getString("WorldResources." + w.getName());
			if (resourceUrl != null){
				p.setResourcePack(resourceUrl);
			}
		}
		if (configB.isSet("WorldGamemode") && configB.getBoolean("WorldGamemode") && config.isSet("worlds")){
			for (String ww : worlds){
				String[] wData = ww.split(",");
				if (wData[0].equalsIgnoreCase(w.getName()) && !wData[3].equalsIgnoreCase("-2")){
					if (!p.isOp() || (configB.isSet("ChangeOpGamemode") && configB.getBoolean("ChangeOpGamemode"))){
						if (wData[3].equalsIgnoreCase("0")){
							p.setGameMode(GameMode.SURVIVAL);
						}
						if (wData[3].equalsIgnoreCase("1")){
							p.setGameMode(GameMode.CREATIVE);
						}
						if (wData[3].equalsIgnoreCase("2")){
							p.setGameMode(GameMode.ADVENTURE);
						}
						if (wData[3].equalsIgnoreCase("3")){
							p.setGameMode(GameMode.SPECTATOR);
						}
					}
				}
			}
		}
	}
}
