package xyz.wylbb.math;

/**Use this to apply some implemented functions to your neural network*/
public enum Functions implements Function{
	
	/**Sigmoid Function*/
	SIGMOID(0),
	/**Hyperbolic tangent Function*/
	TANH(1),
	/**Square Root Function*/
	SQRT(2),
	/**Sine Function*/
	SIN(3);
	
	private int f;
	
	Functions(int function){
		this.f = function;
	}
	
	private double sigmoid(double x){
		return (1/(1 + Math.pow(Math.E,(-1*x))));
	}
	
	private double tanh(double x){
		return 1 - (2/(1 + Math.pow(Math.E,(2*x))));
	}

	@Override
	public double function(double x) {
		switch(this.f){
		case 0:
			return sigmoid(x);
		case 1:
			return tanh(x);
		case 2:
			return Math.sqrt(x);
		case 3:
			return Math.sin(x);
		}
		return 0;
	}
}
