package xyz.wylbb.math;

/**This interface is used to create functions which can be applied to a NeuralNetwork */
public interface Function {
	
	/**Method that returns a double which is the function result when the function is 
	 *applied to the given input*/
	public double function(double x);
	
}
