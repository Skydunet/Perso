package xyz.wylbb.math;

import java.math.BigDecimal;

public enum Counters implements Counter{
	
	DISTANCE(0);
	
	private int t;
	
	Counters(int t){
		this.t = t;
	}
	
	@Override
	public long getScore(BigDecimal[] inputs, BigDecimal[] outputs, BigDecimal[] expectedOutputs){
		switch(this.t){
		case 0:
			long d = 0;
			for (int i = 0; i < outputs.length; i++){
				double b = Double.parseDouble(outputs[i].toPlainString()) - Double.parseDouble(expectedOutputs[i].toPlainString());
				d += Math.abs(b*100000);
			}
			return d;
		}
		return 0;
	}
	
}
