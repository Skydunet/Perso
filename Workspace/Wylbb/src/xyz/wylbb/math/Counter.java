package xyz.wylbb.math;

import java.math.BigDecimal;

public interface Counter {
	
	public long getScore(BigDecimal[] inputs, BigDecimal[] outputs, BigDecimal[] expectedOutputs);
	
}
