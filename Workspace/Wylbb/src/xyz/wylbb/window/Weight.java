package xyz.wylbb.window;

import java.math.BigDecimal;

public class Weight {
	
	public double fromX;
	public double fromY;
	public double toX;
	public double toY;
	public double value;
	public double fromColCount;
	public double toColCount;
	
	public Weight(int fX, int fY, int tX, int tY, BigDecimal v, double fC, double tC){
		this.fromX = fX;
		this.fromY = fY;
		this.toX = tX;
		this.toY = tY;
		this.fromColCount = fC;
		this.toColCount = tC;
		this.value = Double.parseDouble(v.toPlainString());;
	}
	
}
