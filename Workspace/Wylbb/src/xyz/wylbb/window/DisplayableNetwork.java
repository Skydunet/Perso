package xyz.wylbb.window;

import java.math.BigDecimal;

public interface DisplayableNetwork {
	public BigDecimal[][][] getNetwork();
	public BigDecimal[] getInputs();
	public BigDecimal[][] getOutputs(BigDecimal[] inputs);
	public int[] getParameters();
}
