package xyz.wylbb.window;

import java.math.BigDecimal;

public class Neuron {
	
	public double x;
	public double y;
	public double value;
	public int colCount;
	
	public Neuron(double x, double y, BigDecimal v, int c){
		this.x = x;
		this.y = y;
		this.colCount = c;
		this.value = Double.parseDouble(v.toPlainString());
	}
	
}
