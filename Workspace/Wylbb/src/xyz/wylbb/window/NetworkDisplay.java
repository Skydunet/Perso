package xyz.wylbb.window;

import java.awt.Color;
import java.awt.Graphics;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

/**Use this class to display a specified DisplayableNetwork with the last inputs
 * it has been given to it.<br/>
 * This Class extends a JFrame so all JFrame's methods are callable.
 */
public class NetworkDisplay extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private Graphics g;
	private String name;
	private boolean dV;
	/**Variable which sotre the displayed DisplayableNetwork. If you want you
	 * can set another NeuralNetwork by editing this variable*/
	public DisplayableNetwork nn;
	private int x = 500;
	private int y = 500;
	private int res;
	
	/**Use this method to create a new whidow which will be diplaying the given DisplayableNetwork
	 * 
	 * @param neuralNetwork Give the DisplayableNetwork which will be displayed.
	 * @param resolution Gap that will separate the Neurons columns and rows
	 * @param name The Name of the Window
	 * @param displayValues Set to <strong>true</strong> if you want that the Neurons value are displayed on the window.
	 * <br/>Else set to <strong>false</strong>
	 */
	public NetworkDisplay(DisplayableNetwork neuralNetwork, int resolution, String name, boolean displayValues){
		this.nn = neuralNetwork;
		this.dV = displayValues;
		this.name = name;
		this.res = resolution;
	    this.setTitle(this.name);
	    this.setSize(this.x, this.y);
	    this.setLocationRelativeTo(null);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setResizable(false);
	    this.setVisible(true);
	    this.g = this.getGraphics();
	}
	
	@Override
	public void paint(Graphics g){
		//System.out.println(this.g);
		this.g = g;
		this.g.setColor(Color.WHITE);
		this.g.fillRect(0, 0, this.getWidth(), this.getHeight());
		BigDecimal output[][];
		if (this.nn.getInputs() != null){
			output = this.nn.getOutputs(this.nn.getInputs());
		}else{
			int n = this.nn.getParameters()[0];
			BigDecimal[] b = new BigDecimal[n];
			for (int i = 0; i < n; i++){
				b[i] = BigDecimal.ZERO;
			}
			output = this.nn.getOutputs(b);
		}
		List<Neuron> nList = new ArrayList<Neuron>();
		List<Weight> wList = new ArrayList<Weight>();
		for (int x = 0; x < output.length; x++){
			for (int y = 0; y < output[x].length; y++){
				nList.add(new Neuron(x, y, output[x][y], output[x].length));
			}
		}
		for (int a = 0; a < nn.getNetwork().length; a++){
			for (int b = 0; b < nn.getNetwork()[a].length-1; b++){
				for (int c = 0; c < nn.getNetwork()[a][b].length; c++){
					wList.add(new Weight(a, b, a+1, c, nn.getNetwork()[a][b][c], nn.getNetwork()[a].length-1, nn.getNetwork()[a][b].length));
				}
			}
		}
		int maxX = 0;
		int maxY = 0;
		double minN = nList.get(0).value;
		double maxN = nList.get(0).value;
		for (Neuron n : nList){
			if (toPixel(n.x) > maxX){
				maxX = toPixel(n.x);
			}
			if (toPixel(n.y) > maxY){
				maxY = toPixel(n.y);
			}
			if (n.value < minN){
				minN = n.value;
			}
			if (n.value > maxN){
				maxN = n.value;
			}
		}
		maxX += this.res + 30;
		maxY += this.res + 30;
		if (maxX != this.x || maxY != this.y){
			this.x = maxX;
			this.y = maxY;
			this.setSize(maxX, maxY);
		}
		
		double minW = 0;
		double maxW = 0;
		for (Weight w : wList){
			if (w.value < minW){
				minW = w.value;
			}
			if (w.value > maxW){
				maxW = w.value;
			}
		}
		double mmm = maxW - minW;
		for (Weight w : wList){
			w.fromY = w.fromY + ((toCoords(maxY) - w.fromColCount) / 2);
			w.toY = w.toY + ((toCoords(maxY) - w.toColCount) / 2);
			double v = ((w.value-minW)/mmm) * 255;
			//System.out.println(v + ":" + mmm + ":" + w.value + ":" + minW);
			int vv = 255 - (int)(255 - v);//(int)v;
			this.g.setColor(new Color(vv, vv, vv));
			this.g.drawLine(toPixel(w.fromX)+15, toPixel(w.fromY)+14, toPixel(w.toX)+15, toPixel(w.toY)+14);
			this.g.drawLine(toPixel(w.fromX)+15, toPixel(w.fromY)+15, toPixel(w.toX)+15, toPixel(w.toY)+15);
		}
		mmm = maxN - minN;
		for (Neuron n : nList){
			n.y = n.y + ((toCoords(maxY) - n.colCount) / 2);
			double v = ((n.value-minN)/mmm) * 255;
			int vv = (int)(255 - v);
			//System.out.println(vv + ":" + toPixel(n.x) + ":" + toPixel(n.y));
			this.g.setColor(new Color(vv, vv, vv));
			this.g.fillOval(toPixel(n.x), toPixel(n.y), 30, 30);
			this.g.setColor(Color.BLACK);
			this.g.drawOval(toPixel(n.x), toPixel(n.y), 29, 29);
			if (this.dV){
				String s = Double.toString(n.value);
				this.g.drawString(s, toPixel(n.x)+15-((int)(s.length()*3.23)), toPixel(n.y)+45);
			}
		}
	}
	
	private int toPixel(double v){
		return (int)(this.res + (v * this.res));
	}
	
	private double toCoords(int p){
		return (p - this.res) / this.res;
	}
	
}
