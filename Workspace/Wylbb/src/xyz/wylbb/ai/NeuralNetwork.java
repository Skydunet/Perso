package xyz.wylbb.ai;

import java.math.BigDecimal;

import xyz.wylbb.math.Counters;
import xyz.wylbb.math.Function;
import xyz.wylbb.window.DisplayableNetwork;

public class NeuralNetwork implements DisplayableNetwork, Comparable<NeuralNetwork>{
	
	public long score;
	private int[] params;
	private Function function;
	private int pres;
	public BigDecimal[][][] network;
	public BigDecimal[] lastInputs;
	
	public NeuralNetwork(int[] parameters, int precision, Function function){
		this.params = parameters;
		this.score = 0;
		this.pres = precision;
		this.function = function;
		this.network = new BigDecimal[this.params.length-1][][];
		for (int i = 0; i < this.params.length-1; i++){
			this.network[i] = new BigDecimal[this.params[i]+1][this.params[i+1]];
		}
		this.network = rnd(this.network);
	}
	
	public NeuralNetwork(NeuralNetwork nn, NeuralNetwork nn2, double mutationsProbability){
		//onfait une copie mix�e et mut�e
		this.function = nn.getFunction();
		this.score = 0;
		this.params = nn.getParameters();
		this.pres = nn.getPrecision();
		this.network = new BigDecimal[this.params.length-1][][];
		for (int i = 0; i < this.params.length-1; i++){
			this.network[i] = new BigDecimal[this.params[i]+1][this.params[i+1]];
		}
		copy(nn);
		mix(nn2);
		mutate((int)(1 / mutationsProbability));
	}
	
	public BigDecimal[][] getOutputs(BigDecimal[] inputs){
		BigDecimal[][] outputs = new BigDecimal[this.network.length+1][];
		for (int i = 0; i < this.network.length; i++){
			outputs[i+1] = new BigDecimal[this.params[i+1]];
			if (i == 0){
				outputs[0] = inputs;
			}
			for (int j = 0; j < outputs[i+1].length; j++){
				BigDecimal tmp = BigDecimal.ZERO;
				for (int jj = 0; jj < outputs[i].length; jj++){
					tmp = tmp.add(this.network[i][jj][j].multiply(outputs[i][jj]));
				}
				if (this.function != null){
					outputs[i+1][j] = new BigDecimal(this.function.function((Double.parseDouble((tmp.add(this.network[i][this.network[i].length-1][j])).toPlainString()))));
				}else{
					outputs[i+1][j] = tmp.add(this.network[i][this.network[i].length-1][j]);
				}
			}
		}
		return outputs;
	}
	
	public BigDecimal[] ask(BigDecimal[] inputs){
		lastInputs = inputs;
		BigDecimal[][] out = getOutputs(inputs);
		return out[out.length-1];
	}
	
	private void copy(NeuralNetwork nn){
		for (int i = 0; i < nn.network.length; i++){
			for (int ii = 0; ii < nn.network[i].length; ii++){
				for (int iii = 0; iii < nn.network[i][ii].length; iii++){
					this.network[i][ii][iii] = nn.network[i][ii][iii];
				}
			}
		}
	}
	
	private void mix(NeuralNetwork nn){
		for (int i = 0; i < nn.network.length; i++){
			for (int ii = 0; ii < nn.network[i].length; ii++){
				for (int iii = 0; iii < nn.network[i][ii].length; iii++){
					if (Math.random() < 0.5){
						this.network[i][ii][iii] = nn.network[i][ii][iii];
					}
				}
			}
		}
	}
	
	private void mutate(int prob){
		for (int i = 0; i < this.network.length; i++){
			for (int ii = 0; ii < this.network[i].length; ii++){
				for (int iii = 0; iii < this.network[i][ii].length; iii++){
					if ((int)(Math.random() * prob) == 0){
						this.network[i][ii][iii] = random(this.pres);
					}
				}
			}
		}
	}
	
	private BigDecimal[][][] rnd(BigDecimal[][][] tab){
		for (int i = 0; i < tab.length; i++){
			for (int ii = 0; ii < tab[i].length; ii++){
				for (int iii = 0; iii < tab[i][ii].length; iii++){
					tab[i][ii][iii] = random(this.pres);
				}
			}
		}
		return tab;
	}
	
	public void train(BigDecimal[] inputs, BigDecimal[] expectedOutputs, double learningRate){
		for (int o = this.network.length-1; o >= 0; o--){
			for (int oo = 0; oo < this.network[o].length; oo++){
				for (int ooo = 0; ooo < this.network[o][oo].length; ooo++){
					BigDecimal w = this.network[o][oo][ooo];
					BigDecimal l = new BigDecimal(learningRate);
					//double err = sub(ask(inputs), expectedOutputs);//inutile
					this.network[o][oo][ooo] = w.subtract(l);
					double aa = sub(ask(inputs), expectedOutputs);
					this.network[o][oo][ooo] = w.add(l);
					double bb = sub(ask(inputs), expectedOutputs);;
					BigDecimal deriv = new BigDecimal(Math.abs(bb-aa)).divide(new BigDecimal(2 * learningRate), BigDecimal.ROUND_CEILING);
					//BigDecimal deriv = new BigDecimal(err).divide(w, BigDecimal.ROUND_CEILING);
					//this.network[o][oo][ooo] = w.subtract(new BigDecimal(learningRate).multiply(deriv));
					//System.out.println(err + ":" + sub(ask(inputs), expectedOutputs) + ":" + a + ":" + b + ":" + (b-a));
					deriv = BigDecimal.ONE;
					this.network[o][oo][ooo] = w.subtract(new BigDecimal(learningRate).multiply(deriv));
					long a = Counters.DISTANCE.getScore(inputs, ask(inputs), expectedOutputs);
					this.network[o][oo][ooo] = w.add(new BigDecimal(learningRate).multiply(deriv));
					long b = Counters.DISTANCE.getScore(inputs, ask(inputs), expectedOutputs);
					this.network[o][oo][ooo] = w;
					long c = Counters.DISTANCE.getScore(inputs, ask(inputs), expectedOutputs);
					if (a < b && a < c){
						this.network[o][oo][ooo] = w.subtract(new BigDecimal(learningRate));
						System.out.print("a");
					}else if (b < a && b < c){
						this.network[o][oo][ooo] = w.add(new BigDecimal(learningRate));
						System.out.print("b");
					}else{
						this.network[o][oo][ooo] = w;
						System.out.print("c");
					}
				}
			}
		}
	}
	
	private double sub(BigDecimal[] outputs, BigDecimal[] expectedOutputs){
		double d = 0;
		for (int i = 0; i < outputs.length; i++){
			double b = -Double.parseDouble(expectedOutputs[i].toPlainString()) + Double.parseDouble(outputs[i].toPlainString());
			d += b;
		}
		return d;
	}
	
	public Function getFunction(){
		return this.function;
	}
	
	public int getPrecision(){
		return this.pres;
	}
	
	public int[] getParameters(){
		return this.params;
	}
	
	public BigDecimal[][][] getWeights(){
		BigDecimal[][][] out = new BigDecimal[this.network.length][][];
		for (int i = 0; i < this.network.length; i++){
			out[i] = new BigDecimal[this.params[i]][this.params[i+1]];
			for (int ii = 0; ii < this.network[i].length-1; ii++){
				out[i][ii] = this.network[i][ii];
			}
		}
		return out;
	}
	
	public BigDecimal[][] getBias(){
		BigDecimal[][] out = new BigDecimal[this.network.length][];
		for (int i = 0; i < this.network.length; i++){
			out[i] = new BigDecimal[this.params[i+1]];
			for (int ii = 0; ii < this.params[i+1]; ii++){
				out[i][ii] = this.network[i][this.network.length-1][ii];
			}
		}
		return out;
	}
	
	private BigDecimal random(int length){
		long out = (long)(Math.random() * (Math.pow(10, length)));
		return new BigDecimal(Double.toString(((double)(out / (Math.pow(10, length))))));
	}

	@Override
	public int compareTo(NeuralNetwork nn) {
		if (this.score > nn.score){
			return 1;
		}else if (this.score == nn.score){
			return 0;
		}else{
			return -1;
		}
	}

	@Override
	public BigDecimal[][][] getNetwork() {
		return this.network;
	}

	@Override
	public BigDecimal[] getInputs() {
		return this.lastInputs;
	}
	
}
