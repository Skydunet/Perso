package xyz.wylbb.ai;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import xyz.wylbb.math.Counter;
import xyz.wylbb.math.Function;

public class Population {
	
	private int size;
	public NeuralNetwork[] networks;
	private double mP;
	private int pres;
	private Function function;
	private boolean target;//true = +oo   // false = 0
	
	public Population(int size, int[] params, double mutationsProbability, int precision, boolean target, Function function){
		this.size = size;
		this.pres = precision;
		this.target = target;
		this.function = function;
		this.mP = mutationsProbability;
		this.networks = new NeuralNetwork[size];
		for (int i = 0; i < size; i++){
			this.networks[i] = new NeuralNetwork(params, this.pres, this.function);
		}
	}
	
	public Population(Population pop){
		this.size = pop.getSize();
		this.target = pop.getTarget();
		this.networks = new NeuralNetwork[this.size];
		List<NeuralNetwork> bests = new ArrayList<NeuralNetwork>();
		List<NeuralNetwork> copy = new ArrayList<NeuralNetwork>();
		for (NeuralNetwork nn : pop.networks){
			copy.add(nn);
		}
		Collections.sort(copy);
		if (this.target){
			for (int i = this.size/2; i < this.size; i++){
				bests.add(copy.get(i));
			}
		}else{
			for (int i = 0; i < this.size/2; i++){
				bests.add(copy.get(i));
			}
		}
		for (int i = 0; i < this.size; i++){
			if (i < bests.size()){
				this.networks[i] = bests.get(i);
			}else{
				this.networks[i] = new NeuralNetwork(rndNetwork(bests), rndNetwork(bests), this.mP);
			}
		}
	}
	
	public void process(BigDecimal[][] inputs, BigDecimal[][] expectedOutputs, Counter counter){
		for (NeuralNetwork nn : this.networks){
			nn.score = 0;
			for (int i = 0; i < inputs.length; i++){
				BigDecimal[] outputs = nn.ask(inputs[i]);
				nn.score += counter.getScore(inputs[i], outputs, expectedOutputs[i]);
			}
		}
	}
	
	public void upgrade(){
		this.networks = new Population(this).networks;
	}
	
	public NeuralNetwork[] getSortedNetworks(){
		NeuralNetwork[] nList = new NeuralNetwork[this.networks.length];
		for (int i = 0; i < this.networks.length; i++){
			nList[i] = this.networks[i];
		}
		Arrays.sort(nList);
		return nList;
	}
	
	private NeuralNetwork rndNetwork(List<NeuralNetwork> lnn){
		int n = (int)(Math.random() * lnn.size());
		return lnn.get(n);
	}
	
	public int getSize(){
		return this.size;
	}
	
	public boolean getTarget(){
		return this.target;
	}
	
}
