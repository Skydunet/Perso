package test;

import java.awt.Color;
import java.awt.Graphics;
import java.math.BigDecimal;

import javax.swing.JFrame;

import xyz.wylbb.ai.NeuralNetwork;

public class Window extends JFrame{
	
	private static final long serialVersionUID = 2L;
	private NeuralNetwork nn;
	
	public Window(NeuralNetwork nn){
		this.setTitle("fe");
	    this.setSize(300, 300);
	    this.setLocationRelativeTo(null);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setResizable(false);
	    this.setVisible(true);
	    this.nn = nn;
	}
	
	@Override
	public void paint(Graphics g){
		for (int x = 0; x < 11; x++){
			for (int y = 0; y < 11; y++){
				double xx = (double)x/10;
				double yy = (double)y/10;
				BigDecimal[] in = {new BigDecimal(xx), new BigDecimal(yy)};
				int c = (int)((Double.parseDouble(this.nn.ask(in)[0].toPlainString()))*255);
				g.setColor(new Color(c, c, c));
				g.fillRect(20*(x+2), 20*(y+2), 20, 20);
			}
		}
	}
	
}
