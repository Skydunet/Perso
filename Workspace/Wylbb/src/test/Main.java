package test;

import java.math.BigDecimal;

import xyz.wylbb.ai.NeuralNetwork;
import xyz.wylbb.ai.Population;
import xyz.wylbb.math.Counters;
import xyz.wylbb.math.Functions;
import xyz.wylbb.window.NetworkDisplay;

public class Main {
	
	public static void main(String[] args) throws InterruptedException{
		//int[] params = {2, 3, 5, 4, 3, 4, 5, 3};
		int[] params = {2, 3, 2, 1};
		NeuralNetwork nn = new NeuralNetwork(params, 3, Functions.SIGMOID);
		int[][] in = {{0, 0}, {0, 1}, {1, 0}, {1, 1}};
		//int[][] oo = {{0, 0}, {1, 0}, {1, 0}, {0, 0}};
		int[][] oo = {{0}, {1}, {1}, {0}};
		BigDecimal[][] inputs = new BigDecimal[4][2];
		BigDecimal[][] expectedOutputs = new BigDecimal[4][1];
		for (int i = 0; i < in.length; i++){
			for (int ii = 0; ii < in[i].length; ii++){
				inputs[i][ii] = new BigDecimal(in[i][ii]);
			}
		}
		for (int i = 0; i < oo.length; i++){
			for (int ii = 0; ii < oo[i].length; ii++){
				expectedOutputs[i][ii] = new BigDecimal(oo[i][ii]);
			}
		}
		for (int i = 0; i < 1000; i++){
			int rn = (int)(Math.random() * 4);
			for (int j = 0; j < 4; j++){
				nn.train(inputs[j], expectedOutputs[j], 0.05);
			}
			System.out.println(i);
		}
		//nn.train(inputs[0], expectedOutputs[0], 0.02);
		NetworkDisplay nd = new NetworkDisplay(nn, 99, "XOR", true);
		Window w = new Window(nn);
		
		while (true){
			for (int d = 0; d < 4; d++){
				nn.ask(inputs[d]);
				nd.repaint();
				Thread.sleep(5000);
			}
		}
		//Population pop = new Population(2000, params, 0.05, 4, false, Functions.TANH);
		//pop.process(inputs, expectedOutputs, Counters.DISTANCE);
		//NetworkDisplay nd2 = new NetworkDisplay(pop.getSortedNetworks()[0], 99, "pop", true);
		/*System.out.println(pop.getSortedNetworks()[0].score);
		long past = pop.getSortedNetworks()[0].score;
		int id = 0;
		int it = 1;
		boolean stg = false;
		while (true){
			id++;
			if (id == 1000){
				it = 5000;
				nn = nd2.nn;
				stg = true;
				Window w = new Window(pop.getSortedNetworks()[0]);
				int rn = (int)(Math.random() * 4);
				nd2.nn.ask(inputs[rn]);
				nd2.nn = pop.getSortedNetworks()[0];
				nd2.repaint();
				break;
			}
			Thread.sleep(it);
			int rn = (int)(Math.random() * 4);
			if (!stg){
				pop = new Population(pop);
				pop.process(inputs, expectedOutputs, Counters.DISTANCE);
			}
			if (stg){
				nd2.nn.ask(inputs[rn]);
				nd2.nn = pop.getSortedNetworks()[0];
				nd2.repaint();
				past = nd2.nn.score;
			}
			System.out.println(pop.getSortedNetworks()[0].score + ":" + id);
		}*/
		/*BigDecimal[] rslt = nn.ask(inputs);
		String out = "";
		for (BigDecimal bd : rslt){
			out += bd + ":";
		}
		System.out.println(out);*/
		//NetworkDisplay nd = new NetworkDisplay(nn, 50, "Neural Network", false);
	}
	
}
