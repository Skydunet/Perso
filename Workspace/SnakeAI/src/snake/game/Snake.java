package snake.game;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

public class Snake {
	
	public int length;
	private List<int[]> coords = new ArrayList<int[]>();
	private Graphics g;
	public int[] head;
	public boolean lose;
	
	public Snake(Graphics g){
		this.g = g;
		this.length = 4;
		this.lose = false;
		for (int i=0; i < 4; i++){
			int [] tmp = new int[4];
			tmp[0] = 5 + i;
			tmp[1] = 5;
			tmp[2] = 3 - i;
			if (i == 3){
				tmp[3] = 1;
				this.head = tmp;
			}else{
				tmp[3] = 0;
			}
			coords.add(tmp);
		}
	}
	
	public void draw(){
		for (int[] c : this.coords){
			this.g.setColor(Color.WHITE);
			this.g.fillRect(c[0]*20, c[1]*20, 20, 20);
		}
	}
	
	public boolean update(int dir, Apple apple){//1 = haut, 0 = gauche 2 = droite 3 = bas
		boolean state = false;
		List<int[]> tmp = new ArrayList<int[]>();
		for (int[] c : this.coords){
			if (c[3] == 1){
				int[] tt = new int[4];
				switch(dir){
				case 1:
					tt[0] = c[0];
					tt[1] = c[1] - 1;
					tt[2] = 0;
					tt[3] = 1;
					break;
				case 0:
					tt[0] = c[0] - 1;
					tt[1] = c[1];
					tt[2] = 0;
					tt[3] = 1;
					break;
				case 2:
					tt[0] = c[0] + 1;
					tt[1] = c[1];
					tt[2] = 0;
					tt[3] = 1;
					break;
				case 3:
					tt[0] = c[0];
					tt[1] = c[1] + 1;
					tt[2] = 0;
					tt[3] = 1;
					break;
				}
				tmp.add(tt);
				if (c[0] == apple.x && c[1] == apple.y){
					state = true;
					this.length++;
				}
				this.head = tt;
				for (int[] cc : coords){
					if (cc[0] == tt[0] && cc[1] == tt[1]){
						this.lose = true;
					}
				}
			}
			int[] t = new int[4];
			t[0] = c[0];
			t[1] = c[1];
			t[2] = c[2]+1;
			t[3] = 0;
			if (t[2] <= this.length){
				tmp.add(t);
			}
		}
		this.coords = tmp;
		return state;
	}
	
	public int frontDist(int direction, Apple apple){
		int i = 1;
		for (int[] c : this.coords){
			if (c[3] == 1){
				switch(direction){
				case 0:
					while (checkCoords(c[0], c[1] - i, apple) == 0){
						i++;
					}
					return i;
				case 1:
					while (checkCoords(c[0] - i, c[1], apple) == 0){
						i++;
					}
					return i;
				case 2:
					while (checkCoords(c[0] + i, c[1], apple) == 0){
						i++;
					}
					return i;
				case 3:
					while (checkCoords(c[0], c[1] + i, apple) == 0){
						i++;
					}
					return i;
				}
				break;
			}
		}
		return 0;
	}
	
	public int frontBlock(int direction, Apple apple){
		int i = 1;
		for (int[] c : this.coords){
			if (c[3] == 1){
				switch(direction){
				case 0:
					while (checkCoords(c[0], c[1] - i, apple) == 0){
						i++;
					}
					return checkCoords(c[0], c[1] - i, apple);
				case 1:
					while (checkCoords(c[0] - i, c[1], apple) == 0){
						i++;
					}
					return checkCoords(c[0] - i, c[1], apple);
				case 2:
					while (checkCoords(c[0] + i, c[1], apple) == 0){
						i++;
					}
					return checkCoords(c[0] + i, c[1], apple);
				case 3:
					while (checkCoords(c[0], c[1] + i, apple) == 0){
						i++;
					}
					return checkCoords(c[0], c[1] + i, apple);
				}
				break;
			}
		}
		return 0;
	}
	
	private int checkCoords(int x, int y, Apple apple){
		for (int[] a : this.coords){
			if (a[0] == x && a[1] == y){
				return 1;//corps
			}
		}
		if (x == apple.x && y == apple.y){
			return 2;//apple
		}
		if (x == 0 || x == 30 || y == 0 || y == 30){
    		return 3;//wall
    	}
		return 0;//nothing
	}
	
}
