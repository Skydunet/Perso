package snake.game;

import java.awt.Color;
import java.awt.Graphics;

import snake.window.Frame;

public class Apple {
	
	public int x;
	public int y;
	private Graphics g;
	
	public Apple(Frame f){
		this.g = f.g;
		this.x = (int)(Math.random()*((f.getWidth()/20)-2))+1;
		this.y = (int)(Math.random()*((f.getHeight()/20)-2))+1;
	}
	
	public Apple(Frame f, int x, int y){
		this.g = f.g;
		this.x = x;
		this.y = y;
	}
	
	public void draw(){
		this.g.setColor(Color.RED);
		this.g.fillRect(this.x*20, this.y*20, 20, 20);
	}
	
}
