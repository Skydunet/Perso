package snake.main;

import java.awt.Color;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextField;

import snake.ai.AI;
import snake.ai.FoundEntity;
import snake.ai.Population;
import snake.game.Apple;
import snake.game.Snake;
import snake.window.DataWindow;
import snake.window.Frame;

public class Main {
	
	private static int direction = 2;
	private static Apple apple;
	private static Snake snake;
	private static int pts;
	private static long time;
	private static Population pop;
	private static int pops = 0;
	private static int popSize = 1000;
	private static AI ai;
	private static AI aiT = null;
	private static int index = 0;
	private static JLabel label = new JLabel();
	private static JTextField jtf = new JTextField();;
	private static JTextField jtf2 = new JTextField();
	private static JTextField jtf3 = new JTextField();
	private static JTextField jtf4 = new JTextField();
	private static FoundEntity[] fe = new FoundEntity[3];
	private static long loses = 0;
	private static boolean test = false;
	private static int[] patternX = {10, 25, 3, 9, 15};
	private static int[] patternY = {20, 4, 12, 16, 20};
	private static int patternI = 0;
	private static boolean appleP = false;
	private static long moy = 0;
	private static long moyC = 0;
	private static long bestScore = 0;
	private static long bestAll = 0;
	private static long ageLastApple = 0;
	
	public static void main(String[] args) throws InterruptedException{
	    Frame frame = new Frame();
	    DataWindow dw = new DataWindow();
	    pop = new Population(popSize);
	    ai = pop.ais[0];
	    dw.pan.add(label);
	    dw.pan.add(jtf);
	    dw.pan.add(jtf2);
	    dw.pan.add(jtf3);
	    dw.pan.add(jtf4);
	    jtf.setText("100");//speed
	    jtf2.setText("8");//mutations
	    jtf3.setText("0");//stop
	    jtf4.setText("9");//virgule
	    pts = 0;
	    time = 0;
	    Thread.sleep(200);
    	snake = new Snake(frame.g);
    	newApple(frame);
	    while(true){
	    	if (test){
	    		Thread.sleep(Integer.parseInt(jtf.getText()));
	    	}
	    	writeDW();
	    	frame.g.setColor(Color.BLACK);
	    	frame.g.fillRect(0, 0, frame.getWidth(), frame.getHeight());
	    	if (snake.update(direction, apple)){
	    		newApple(frame);
	    		ageLastApple = time;
	    		pts++;
	    	}
	    	if (snake.head[0] <= 0 || snake.head[0] >= frame.getWidth()/20 || snake.head[1] <= 0 || snake.head[1] >= frame.getHeight()/20){
	    		lose(frame);
	    	}
	    	if (snake.lose){
	    		lose(frame);
	    	}
	    	if (Integer.parseInt(jtf3.getText()) != 0 || (time - ageLastApple) > 200){
	    		lose(frame);
	    	}
	    	if (test){
	    		snake.draw();
	    		apple.draw();
	    	}
	    	time++;
	    	if (aiT != null){
	    		ai = aiT;
	    	}
	    	findEntities();
	    	changeDirection(ai.ask(fe));
	    }
	}
	
	private static void changeDirection(int dir){
		direction += dir;
		if (direction == -1){
			direction = 3;
		}else if (direction == 4){
			direction = 0;
		}
	}
	
	private static void lose(Frame frame){
		patternI = 0;
		direction = 2;
		if (!test){
			loses++;
			index++;
			ai.score = pts*100 + time/2;
			moyC += ai.score;
		}
		pts = 0;
		time = 0;
		ageLastApple = 0;
		if (index >= pop.size || test){//on a test tt les ai//we'd tried all AIs
			if (test){
				index = 0;
				pops++;
				test = false;
				pop = new Population(pop, Integer.parseInt(jtf2.getText()), Integer.parseInt(jtf4.getText()));
			}else{
				test = true;
				ai = pop.getBest();
				for (AI a : pop.ais){//a laisser car il y a souvent des pb de calcul du meilleur
					if (a.score > ai.score){
						ai = a;
					}
				}
				bestScore = ai.score;
				if (bestScore > bestAll){
					bestAll = bestScore;
				}
				snake = new Snake(frame.g);
				newApple(frame);
				moy = moyC / popSize;
				moyC = 0;
			}
		}
		if (!test){
			ai = pop.ais[index];
			snake = new Snake(frame.g);
			newApple(frame);
		}
	}
	
	private static void writeDW(){
		String st = "<html>";
		for (int i = 0; i < ai.first[0].length; i++){
			st = st + "<br/>" + ai.first[0][i];
		}
		st += "<br/><br/>" + loses + "<br/>" + pops + "<br/>" + ai.score + "<br/>" + moy + "<br/>" + bestScore + "<br/>" + bestAll + "</html>";
		label.setText(st);
	}
	
	private static void findEntities(){
		for (int i = 0; i < 3; i++){
			fe[i] = new FoundEntity(snake.frontBlock(checkDirection(i-1), apple), snake.frontDist(checkDirection(i-1), apple));
		}
	}
	
	private static int checkDirection(int d){
		if (d == -1){
			return 3;
		}else if (d == 4){
			return 0;
		}else{
			return d;
		}
	}
	
	private static void newApple(Frame frame){
		if (appleP){
			apple = new Apple(frame, patternX[patternI], patternY[patternI]);
			patternI++;
		}else{
			apple = new Apple(frame);
		}
	}
	
	private static void saveData(){
		String data = "";
		String path = "D:/ai.txt";
		for (AI ai : pop.ais){
			for (BigDecimal[] a : ai.first){
				for (BigDecimal b : a){
					data += b + ":";
				}
				data += "\n";
			}
			data += "\n\n";
		}
		FileSaver.save(path, data);
	}
	
}
