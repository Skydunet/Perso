package snake.window;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DataWindow extends JFrame {
	
	public JPanel pan;
	public Graphics g;
	
	public DataWindow(){
		pan = new JPanel();
	    this.setTitle("Snake Data");
	    this.setSize(600, 600);
	    this.setLocationRelativeTo(null);
	    pan.setBackground(Color.WHITE);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setContentPane(pan);        
	    this.setVisible(true);
	    this.g = this.getGraphics();
	}
	
}
