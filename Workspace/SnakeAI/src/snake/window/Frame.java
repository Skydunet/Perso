package snake.window;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Frame extends JFrame{
	
	private JPanel pan;
	public Graphics g;
	
	public Frame(){
		pan = new JPanel();
	    this.setTitle("Snake");
	    this.setSize(600, 600);
	    this.setLocationRelativeTo(null);
	    pan.setBackground(Color.BLACK);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setContentPane(pan);        
	    this.setVisible(true);
	    this.g = this.getGraphics();
	}
}
