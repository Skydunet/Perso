package snake.ai;

import java.math.BigDecimal;
import java.util.List;

public class AI implements Comparable<AI>{
	
	public BigDecimal[][] first;
	public BigDecimal[][] second;
	public BigDecimal[][] third;
	public long score;
	public List<String> path;
	
	public AI(){
		this.first = new BigDecimal[6][10];
		this.second = new BigDecimal[10][10];
		this.third = new BigDecimal[10][3];
		this.first = rnd(this.first);
		this.second = rnd(this.second);
		this.third = rnd(this.third);
		this.score = 0;
	}
	
	private BigDecimal[][] rnd(BigDecimal[][] tab){
		BigDecimal[][] out = new BigDecimal[tab.length][tab[0].length];
		for (int i = 0; i < tab.length; i++){
			for (int ii = 0; ii < tab[i].length; ii++){
				out[i][ii] = random(9);//nb de chiffres apr�s la virgule
			}
		}
		return out;
	}
	
	public int ask(FoundEntity[] fe){
		BigDecimal[] fL = new BigDecimal[this.second.length];
		BigDecimal[] sL = new BigDecimal[this.third.length];
		BigDecimal[] tL = new BigDecimal[3];
		for (int j = 0; j < this.first.length/2; j++){
			for (int i = 0; i < fL.length; i++){
				//fL[i] += fe[j].type * this.first[0+j][i] + fe[j].dist * this.first[1+j][i];
				if (j == 0){
					fL[i] = BigDecimal.ZERO;
				}
				BigDecimal a = this.first[j][i].multiply(new BigDecimal(fe[j].type));
				BigDecimal b = this.first[j+1][i].multiply(new BigDecimal(fe[j].dist));
				/*System.out.println(a.toPlainString() + ":" + b.toPlainString());
				System.out.println(a.add(b));*/
				fL[i] = fL[i].add(a.add(b));
			}
		}
		for (int i = 0; i < sL.length; i++){
			BigDecimal tmp = BigDecimal.ZERO;
			for (int ii = 0; ii < fL.length; ii++){
				//tmp += this.second[ii][i] * fL[i];
				tmp = tmp.add(this.second[ii][i]).multiply(fL[i]);
			}
			sL[i] = tmp;
		}
		for (int i = 0; i < tL.length; i++){
			BigDecimal tmp = BigDecimal.ZERO;
			for (int ii = 0; ii < sL.length; ii++){
				//tmp += this.second[ii][i] * fL[i];
				tmp = tmp.add(this.second[ii][i]).multiply(sL[i]);
			}
			tL[i] = tmp;
		}
		int index = 0;
		BigDecimal value = tL[0];
		for (int i = 1; i < 3; i++){
			if (tL[i].compareTo(value) == 1){//sL[i] > value
				index = i;
				value = tL[i];
			}
		}
		return index - 1;//direction
	}
	
	public void mutate(int prop, int vir){
		for (int i = 0; i < this.first.length; i++){
			for (int ii = 0; ii < this.first[i].length; ii++){
				if ((int)(Math.random()*prop) == 0){
					this.first[i][ii] = random(vir);
				}
			}
		}
		for (int i = 0; i < this.second.length; i++){
			for (int ii = 0; ii < this.second[i].length; ii++){
				if ((int)(Math.random()*prop) == 0){
					this.second[i][ii] = random(vir);
				}
			}
		}
		for (int i = 0; i < this.third.length; i++){
			for (int ii = 0; ii < this.third[i].length; ii++){
				if ((int)(Math.random()*prop) == 0){
					this.third[i][ii] = random(vir);
				}
			}
		}
	}
	
	public void mix(AI ai){
		for (int i = 0; i < this.first.length; i++){
			for (int ii = 0; ii < this.first[i].length; ii++){
				if (Math.random() < 0.5){
					this.first[i][ii] = ai.first[i][ii];
				}
			}
		}
		for (int i = 0; i < this.second.length; i++){
			for (int ii = 0; ii < this.second[i].length; ii++){
				if (Math.random() < 0.5){
					this.second[i][ii] = ai.second[i][ii];
				}
			}
		}
		for (int i = 0; i < this.third.length; i++){
			for (int ii = 0; ii < this.third[i].length; ii++){
				if (Math.random() < 0.5){
					this.third[i][ii] = ai.third[i][ii];
				}
			}
		}
	}
	
	public void copy(AI ai){
		for (int i = 0; i < this.first.length; i++){
			for (int ii = 0; ii < this.first[i].length; ii++){
				this.first[i][ii] = ai.first[i][ii];
			}
		}
		for (int i = 0; i < second.length; i++){
			for (int ii = 0; ii < second[i].length; ii++){
				this.second[i][ii] = ai.second[i][ii];
			}
		}
		for (int i = 0; i < third.length; i++){
			for (int ii = 0; ii < third[i].length; ii++){
				this.third[i][ii] = ai.third[i][ii];
			}
		}
	}
	
	private BigDecimal random(int length){
		long out = (long)(Math.random() * (Math.pow(10, length)));
		return new BigDecimal(Double.toString(((double)(out / (Math.pow(10, length))))));
	}

	@Override
	public int compareTo(AI ai) {
		if (this.score > ai.score){
			return 1;
		}else if (this.score == ai.score){
			return 0;
		}else{
			return -1;
		}
	}
	
}
