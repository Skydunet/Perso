package snake.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Population {
	
	public int size;
	public AI[] ais;
	
	public Population(int size){
		this.size = size;
		this.ais = new AI[size];
		for (int i = 0; i < size; i++){
			ais[i] = new AI();
		}
	}
	
	public Population(Population pop, int mutations, int vir){
		this.size = pop.size;
		this.ais = new AI[pop.size];
		List<AI> best = new ArrayList<AI>();
		List<AI> copy = new ArrayList<AI>();
		for (AI ai : pop.ais){
			copy.add(ai);
		}
		Collections.sort(copy);
		for (int i = 500; i < 1000; i++){
			best.add(copy.get(i));
		}
		for (int i = 0; i < this.size; i++){
			if (i < best.size()){
				this.ais[i] = best.get(i);
			}else{
				this.ais[i] = new AI();
				this.ais[i].copy(randomAI(best));
				this.ais[i].mix(randomAI(best));
				this.ais[i].mutate(mutations, vir);
			}
		}
	}
	
	private AI randomAI(List<AI> aiList){
		int n = (int)(Math.random() * aiList.size());
		return aiList.get(n);
	}
	
	public AI getBest(){
		long score = this.ais[0].score;
		AI best = this.ais[0];
		for (AI a : this.ais){
			if (a.score > score){
				best = a;
			}
		}
		for (AI a : this.ais){
			if (a.score > score){
				best = a;
			}
		}
		return best;
	}
	
}
