package snake.ai;

public class FoundEntity {
	
	public int type;
	public int dist;
	
	public FoundEntity(int type, int dist){
		this.type = type;
		this.dist = dist;
	}
	
}
