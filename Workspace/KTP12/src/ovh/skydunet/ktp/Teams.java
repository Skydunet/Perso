package ovh.skydunet.ktp;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class Teams {
	
	private String name;
	private ChatColor color;
	private Team team;
	public List<Player> players;
	private List<Player> playersDead;
	
	
	public Teams(String name, ChatColor color, Scoreboard sc){
		this.name = name;
		this.color = color;
		
	    sc.registerNewTeam(this.name);
	    
	    Team t = sc.getTeam(this.name);
	    t.setDisplayName(this.name);
	    t.setCanSeeFriendlyInvisibles(true);
	    t.setPrefix(this.color + "");
	    
	    this.team = t;
	}

	public boolean getAlivePlayers(){
		boolean out = true;
		for (Player p : players){
			if (p.isOnline()){
				boolean sta = true;
				for (Player pld : playersDead){
					if (p == pld){
						sta = false;
					}
				}
				if (!sta){
					out = false;
				}
			}
		}
		return out;
	}
	
	public void addPlayer(Player p){
		this.team.addPlayer(p);
	}
	
	public void playerDie(Player p){
		playersDead.add(p);
	}
	
}
