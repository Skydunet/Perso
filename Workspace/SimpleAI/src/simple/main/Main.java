package simple.main;

import simple.ai.AI;
import simple.ai.Population;
import simple.window.MainWindow;

public class Main {//le but est de trouver le nb de 0 dans un nombre de 3 chiffres
	
	private static MainWindow window;
	private static Population pop;
	
	public static void main(String[] args) throws InterruptedException{
		window = new MainWindow();
		pop = new Population(500);
		while(true){
			Thread.sleep(200);
			testPopulation();
			pop = new Population(pop, 250, 9);
		}
	}
	
	private static void testPopulation(){
		String nb = generateString();
		for (AI ai : pop.ais){
			double[] v = {Integer.parseInt(nb.substring(0, 2))/10, Integer.parseInt(nb.substring(1, 3))/10};
			ai.score = Math.abs(3 - ai.ask(v));
		}
	}
	
	private static String generateString(){
		return Integer.toString((int)(Math.random() * 1000));
	}
}
