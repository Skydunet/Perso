package simple.ai;

import java.util.ArrayList;
import java.util.List;

public class Population {
	
	public int size;
	public AI[] ais;
	
	public Population(int size){
		this.size = size;
		this.ais = new AI[size];
		for (int i = 0; i < size; i++){
			ais[i] = new AI();
		}
	}
	
	public Population(Population pop, int mutations, int vir){
		this.size = pop.size;
		this.ais = new AI[pop.size];
		int mScore = 0;
		List<AI> best = new ArrayList<AI>();
		for (AI ai : pop.ais){
			mScore += ai.score;
		}
		mScore /= pop.size;
		for (AI ai : pop.ais){
			if (ai.score <= mScore){
				best.add(ai);
			}
		}
		for (int i = 0; i < this.size; i++){
			if (i < best.size()){
				this.ais[i] = best.get(i);
			}else{
				this.ais[i] = new AI();
				this.ais[i].copy(randomAI(best));
				this.ais[i].mix(randomAI(best));
				this.ais[i].mutate(mutations, vir);
			}
		}
	}
	
	private AI randomAI(List<AI> aiList){
		int n = (int)(Math.random() * aiList.size());
		return aiList.get(n);
	}
	
	public AI getBest(){
		long score = ais[0].score;
		AI best = ais[0];
		for (int i = 1; i < ais.length; i++){
			if (ais[i].score < score){
				best = ais[i];
			}
		}
		return best;
	}
	
}
