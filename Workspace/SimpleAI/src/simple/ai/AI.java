package simple.ai;

public class AI {
	
	public double[][] first;
	public double[][] second;
	public long score;
	
	public AI(){
		this.first = new double[2][8];
		this.second = new double[8][4];
		this.first = rnd(this.first);
		this.second = rnd(this.second);
		this.score = 0;
	}
	
	private double[][] rnd(double[][] tab){
		double[][] out = new double[tab.length][tab[0].length];
		for (int i = 0; i < tab.length; i++){
			for (int ii = 0; ii < tab[i].length; ii++){
				out[i][ii] = Math.random();
			}
		}
		return out;
	}
	
	public int ask(double[] fe){
		double[] fL = new double[8];
		double[] sL = new double[4];
		for (int i = 0; i < 8; i++){
			fL[i] += fe[0] * this.first[0][i] + fe[1] * this.first[1][i];
		}
		for (int i = 0; i < 4; i++){
			double tmp = 0;
			for (int ii = 0; ii < 8; ii++){
				tmp += this.second[ii][i] * fL[i];
			}
			sL[i] = tmp;
		}
		int index = 0;
		double value = sL[0];
		for (int i = 1; i < 4; i++){
			if (sL[i] > value){
				index = i;
				value = sL[i];
			}
		}
		return index;//
	}
	
	public void mutate(int prop, int vir){
		for (int i = 0; i < first.length; i++){
			for (int ii = 0; ii < first[i].length; ii++){
				if ((int)(Math.random()*prop) == 0){
					first[i][ii] = random(vir);
				}
			}
		}
		for (int i = 0; i < second.length; i++){
			for (int ii = 0; ii < second[i].length; ii++){
				if ((int)(Math.random()*prop) == 0){
					second[i][ii] = random(vir);
				}
			}
		}
	}
	
	public void mix(AI ai){
		for (int i = 0; i < first.length; i++){
			int middle = (int)(Math.random() * first[i].length);
			for (int ii = 0; ii < first[i].length; ii++){
				if (ii < middle){
					first[i][ii] = ai.first[i][ii];
				}
			}
		}
		for (int i = 0; i < second.length; i++){
			int middle = (int)(Math.random() * second[i].length);
			for (int ii = 0; ii < second[i].length; ii++){
				if (ii < middle){
					second[i][ii] = ai.second[i][ii];
				}
			}
		}
	}
	
	public void copy(AI ai){
		for (int i = 0; i < first.length; i++){
			for (int ii = 0; ii < first[i].length; ii++){
				this.first[i][ii] = ai.first[i][ii];
			}
		}
		for (int i = 0; i < second.length; i++){
			for (int ii = 0; ii < second[i].length; ii++){
				this.second[i][ii] = ai.second[i][ii];
			}
		}
	}
	
	private double random(int length){
		long out = (long)(Math.random() * (Math.pow(10, length)));
		return (double)(out / (Math.pow(10, length)));
	}
	
}
