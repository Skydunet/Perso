package ovh.skydunet.commandcube.world;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Portal {
	
	public Location loc, destination;
	private List<Portal> portals;
	
	public Portal(Location destination, Location loc){
		this.loc = loc;
		this.destination = destination;
		portals.add(this);
	}//utilser pour le tp le on portal travel;
	
	public void playerMove(Location loc, Player player){
		for (Portal p : portals){
			if (p.loc == loc){
				MovePlayer mover = new MovePlayer();
				mover.moveToCoords(player, p.destination.getWorld().getName(), (int)p.destination.getX(), (int)p.destination.getY(), (int)p.destination.getZ());
			}
		}
	}
	
}
