package ovh.skydunet.commandcube.world;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class MovePlayer {
	
	public MovePlayer(){

	}
	
	public void moveToSpawn(Player player, String worldName){
		Location loc = new Location(Bukkit.getWorld(worldName), Bukkit.getWorld(worldName).getSpawnLocation().getX(), Bukkit.getWorld(worldName).getSpawnLocation().getY(), Bukkit.getWorld(worldName).getSpawnLocation().getZ());
    	player.teleport(loc);
	}
	
	public void moveToCoords(Player player, String worldName, int x, int y, int z){
		Location loc = new Location(Bukkit.getWorld(worldName), x, y, z);
    	player.teleport(loc);
	}
}
