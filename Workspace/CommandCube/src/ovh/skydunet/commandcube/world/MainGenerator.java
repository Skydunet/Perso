package ovh.skydunet.commandcube.world;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.CommandSender;

public class MainGenerator {
	
	private String name;
	private WorldCreator world;
	
	public MainGenerator(String n){
		this.name = n;
		this.world = new WorldCreator(this.name);
	}
	
	public void generate(CommandSender sender){
		try{
			Bukkit.createWorld(this.world);
			sender.sendMessage(ChatColor.AQUA + "World " + this.name + " Generated");
		}catch(Error e){
			Bukkit.getLogger().info(e.toString());
		}
	}
	
	public void generate(){
		try{
			Bukkit.createWorld(this.world);
		}catch(Error e){
			Bukkit.getLogger().info(e.toString());
		}
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setType(WorldType wt){
		this.world.type(wt);
	}
	
	public void setEnv(Environment ev){
		this.world.environment(ev);
	}
	
	public void setStructureState(boolean st){
		this.world.generateStructures(st);
	}
	
	public void setGenerator(String c){
		if (c == "s"){
			this.world.generator(new WorldGen());
		}else if (c == "a"){
			this.world.generator(new AirWorld());
			this.world.type(WorldType.FLAT);
		}else if (c == "rf"){
			this.world.generator(new RandomFlatWorld());
			this.world.type(WorldType.FLAT);
		}else if (c == "sp"){
			this.world.generator(new SpaceWorld());
			this.world.type(WorldType.FLAT);
		}
	}
	
}
