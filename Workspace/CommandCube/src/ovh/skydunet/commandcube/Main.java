package ovh.skydunet.commandcube;

import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import ovh.skydunet.commandcube.games.Games;
import ovh.skydunet.commandcube.world.MainGenerator;
import ovh.skydunet.commandcube.world.MovePlayer;
import ovh.skydunet.commandcube.world.Portal;

public class Main extends JavaPlugin implements Listener{
	
	private Games games;
	
	@Override
    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getLogger().info("CommandCube enabled");
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("game")){//faire la commande de help
			if (args.length >= 1){
				if (args[0].equalsIgnoreCase("create")){
					if (args.length == 1){
						sender.sendMessage(ChatColor.RED + "You need to specify a name for your creation");
					}else{
						String gameName = "";
						int scanState = 0;
						for (String i : args){
							if (scanState == 1){
								gameName += i;
							}else if (scanState >=2){
								gameName += " " + i;
							}
							scanState++;
						}
						boolean createdResult = games.newGame(gameName);
						if (!createdResult){
							sender.sendMessage(ChatColor.RED + "You alredy use this name for a Game");
						}
					}
					return true;
				}
			}
		}
		if (cmd.getName().equalsIgnoreCase("world")){//faire la commande de help
			if (args.length >= 1){
				if (args[0].equalsIgnoreCase("create")){
					if (args.length == 4 || args.length == 3){
						MainGenerator mainGenerator = new MainGenerator(args[1]);
						WorldType type;
						Environment env;
						if (args[2].equalsIgnoreCase("flat")){
							env = Environment.NORMAL;
							type = WorldType.FLAT;
						}else if (args[2].equalsIgnoreCase("normal")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("amplified")){
							type = WorldType.AMPLIFIED;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("large")){
							type = WorldType.LARGE_BIOMES;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("normal")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
						}else if (args[2].equalsIgnoreCase("nether")){
							type = WorldType.NORMAL;
							env = Environment.NETHER;
						}else if (args[2].equalsIgnoreCase("end")){
							type = WorldType.NORMAL;
							env = Environment.THE_END;
						}else if (args[2].equalsIgnoreCase("cobble")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("s");
						}else if (args[2].equalsIgnoreCase("air")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("a");
						}else if (args[2].equalsIgnoreCase("random")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("rf");
						}else if (args[2].equalsIgnoreCase("space")){
							type = WorldType.NORMAL;
							env = Environment.NORMAL;
							mainGenerator.setGenerator("sp");
						}else{
							sender.sendMessage(ChatColor.RED + "This presset does not exeists");
							return true;
						}
						mainGenerator.setType(type);
						mainGenerator.setEnv(env);
						boolean structure;
						if (args.length == 4){
							if (args[3].equalsIgnoreCase("true")){
								structure = true;
							}else{
								structure = false;
							}
						}else{
							structure = true;
						}
						mainGenerator.setStructureState(structure);
						mainGenerator.generate(sender);
						//Bukkit.createWorld(new WorldCreator(args[2]));
					}else{
						sender.sendMessage(ChatColor.RED + "/world create <WorldName> <WorldGeneration> [generateStructures]");
					}
					return true;
				}else if (args[0].equalsIgnoreCase("move")){
					MovePlayer mover = new MovePlayer();
					Player player = null;
					if (args.length >= 2){
						for (Player p : Bukkit.getOnlinePlayers()){
							if (p.getName().equalsIgnoreCase(args[1])){
								player = p;
							}
						}
					}else{
						sender.sendMessage(ChatColor.RED + "You need to specify a player");
						return true;
					}
					if (args.length == 3){
						if (player != null){
							boolean found = false;
							List<World> worlds = Bukkit.getWorlds();
							for (World world : worlds){
								if (world.getName().equalsIgnoreCase(args[2])){
									found = true;
								}
							}
							if (found){
								mover.moveToSpawn(player, args[2]);
								sender.sendMessage(ChatColor.AQUA + player.getName() + " is moved to the world " + args[2] + " !");
								return true;
							}else{
								sender.sendMessage(ChatColor.RED + "Unable to find the world " + args[2]);
								return true;
							}
						}else{
							sender.sendMessage(ChatColor.RED + "Unable to find the player " + args[1]);
							return true;
						}
					}else if (args.length == 6){
						if (player != null){
							boolean found = false;
							List<World> worlds = Bukkit.getWorlds();
							for (World world : worlds){
								if (world.getName().equalsIgnoreCase(args[2])){
									found = true;
								}
							}
							if (found){
								mover.moveToCoords(player, args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]));
								sender.sendMessage(ChatColor.AQUA + player.getName() + " is moved to the world " + args[2] + " !");
								return true;
							}else{
								sender.sendMessage(ChatColor.RED + "Unable to find the world " + args[2]);
								return true;
							}
						}else{
							sender.sendMessage(ChatColor.RED + "Unable to find the player " + args[1]);
							return true;
						}
					}else{
						sender.sendMessage(ChatColor.RED + "You need to precize World & Player that you want to move");
						return true;
					}
				}else if (args[0].equalsIgnoreCase("portal")){
					if (args.length == 5){
						String name = sender.getName();
						Player pl = null;
						for (Player p : Bukkit.getOnlinePlayers()){
							if (p.getName() == name){
								pl = p;
							}
						}
						World destWorld = null;
						for (World w : Bukkit.getWorlds()){
							if (w.getName() == args[1]){
								destWorld = w;
							}
						}
						if (destWorld != null){
							Location portalLoc = pl.getTargetBlock((Set<Material>)null, 20).getLocation();
							new Portal(new Location(destWorld, (double)Integer.parseInt(args[2]), (double)Integer.parseInt(args[3]), (double)Integer.parseInt(args[4])), portalLoc);
						}else{
							sender.sendMessage(ChatColor.RED + "This world dosen't exists !");
						}
					}else{
						sender.sendMessage(ChatColor.RED + "/world portal <WorldName> <x> <y> <z>");
					}
					return true;
				}
			}
		}
		return false;
	}
	
	public void update(){
		Bukkit.getScheduler().runTaskTimer(this, new Runnable(){
			@Override
			public void run() {
				
			}
		}, 0, 20);
	}
	
	@Override
    public void onDisable(){//save toutes les donn�es enregistr�es dans les variables genre les portals
        
    }
}
