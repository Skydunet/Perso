package fr.skydunet.main;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Frame extends JFrame{
	
	private Pan pan;
	private int x = 0;
	private int y = 0;
	private boolean xS = true;
	private boolean yS = true;
	
	public Frame(){
		pan = new Pan();
	    this.setTitle("Un truc bien styl� !");
	    //D�finit sa taille : 400 pixels de large et 100 pixels de haut
	    this.setSize(400, 400);
	    //Nous demandons maintenant � notre objet de se positionner au centre
	    this.setLocationRelativeTo(null);
	    pan.setBackground(Color.CYAN);
	    //Termine le processus lorsqu'on clique sur la croix rouge
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setContentPane(pan);
	    //Et enfin, la rendre visible        
	    this.setVisible(true);
	}

	public void update() {
		if (x >= pan.getWidth() - 50 && xS == true){
			xS = false;
			x+=2;
		}
		if (x <= 0 && xS == false){
			xS = true;
			x+=2;
		}
		if (y >= pan.getHeight() - 50 && yS == true){
			yS = false;
		}
		if (y <= 0 && yS == false){
			yS = true;
		}
		if (xS == true){
			x++;
		}else{
			x--;
		}
		if (yS == true){
			y++;
		}else{
			y--;
		}
		pan.setPosX(x);
		pan.setPosY(y);
		pan.repaint();
	}
}
