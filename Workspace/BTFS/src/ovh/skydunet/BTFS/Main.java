package ovh.skydunet.BTFS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	
	private FileConfiguration config;
	private FileConfiguration configB;
	private String form = "";
	
	@Override
    public void onEnable(){//ajouter la commande /money
        Bukkit.getPluginManager().registerEvents(this, this);
        File fichierConfig = new File("plugins/BTFS/database.yml");
        File fichierConfig2 = new File("plugins/BTFS/config.yml");
        config = YamlConfiguration.loadConfiguration(fichierConfig);
        configB = YamlConfiguration.loadConfiguration(fichierConfig2);
        if (configB.get("web") == null){
        	configB.set("web", false);
        }
        if (configB.get("webFloder") == null){
        	configB.set("webFloder", "/index.html");
        }
        if (configB.get("webForm") == null){
        	configB.set("webForm", "/form.html");
        }
        
        BufferedReader br = null;
		FileReader fr = null;
		try {
			fr = new FileReader(configB.getString("webForm"));
			br = new BufferedReader(fr);

			String sCurrentLine;

			br = new BufferedReader(new FileReader(configB.getString("webForm")));

			while ((sCurrentLine = br.readLine()) != null) {
				form = form + sCurrentLine + "\n";
			}
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try {
				if (br != null){
					br.close();
				}
				if (fr != null){
					fr.close();
				}
			}catch (IOException ex){
				ex.printStackTrace();
			}
		}

        try{
			configB.save(fichierConfig2);
		}catch (IOException e){
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
    }
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("sell")){
			for (Player p : Bukkit.getOnlinePlayers()){
				if (p.getName() == sender.getName() && p.getLocation().distance(p.getWorld().getSpawnLocation()) <= 20 && p.getInventory().getItemInMainHand().getTypeId() != 0){
					ItemStack it = p.getInventory().getItemInMainHand();
					String itData;
					if ((it.getTypeId() >= 345 && it.getTypeId() <= 347) || (it.getTypeId() >= 256 && it.getTypeId() <= 317) || it.getTypeId() == 359){
						itData = it.getType().toString();
					}else{
						itData = it.getData().toString();
					}
					if (config.get("items." + itData) != null){
						config.set("items." + itData + ".amount", (it.getAmount() + config.getInt("items." + itData + ".amount")));
						if (config.get("players." + p.getName()) != null){
							config.set("players." + p.getName(), config.getInt("players." + p.getName()) + (it.getAmount() * config.getInt("items." + itData + ".price")));
							sender.sendMessage(ChatColor.AQUA + "Vous venez de gagner " + (it.getAmount() * config.getInt("items." + itData + ".price")) + " Coins");
						}else{
							config.set("players." + p.getName(), (it.getAmount() * config.getInt("items." + itData + ".price")));
							sender.sendMessage(ChatColor.AQUA + "Vous venez de gagner " + (it.getAmount() * config.getInt("items." + itData + ".price")) + " Coins");
							addPlayerToList(p.getName());
						}
					}else{
						int total = 0;
						int nb = 0;
						for (String o : config.getStringList("list")){
							total += config.getInt("items." + o + ".amount");
							nb++;
						}
						int moy;
						if (nb != 0){
							moy = (total / nb) * 100;
						}else{
							moy = 100;
						}
						config.set("items." + itData + ".amount", it.getAmount());
						config.set("items." + itData + ".id", it.getTypeId());
						config.set("items." + itData + ".dammage", it.getDurability());
						if (config.get("players." + p.getName()) != null){
							config.set("players." + p.getName(), config.getInt("players." + p.getName()) + (it.getAmount() * moy));
							sender.sendMessage(ChatColor.AQUA + "Vous venez de gagner " + (it.getAmount() * moy) + " Coins");
						}else{
							config.set("players." + p.getName(), (it.getAmount() * moy));
							sender.sendMessage(ChatColor.AQUA + "Vous venez de gagner " + (it.getAmount() * moy) + " Coins");
							addPlayerToList(p.getName());
						}
						List<String> lst = config.getStringList("list");
						lst.add(itData.toString());
						config.set("list", lst);
					}
					p.getInventory().setItemInMainHand(null);
					int total = 0;
					int nb = 0;
					for (String o : config.getStringList("list")){
						total += config.getInt("items." + o + ".amount");
						nb++;
					}
					double moy = total / nb;
					for (String o : config.getStringList("list")){
						config.set("items." + o + ".price", (moy / (config.getInt("items." + o + ".amount") + 1)) * 100);
					}
					if (configB.getBoolean("web")){
						updateWeb(moy * 100);
					}
				}else if (p.getName() == sender.getName() && p.getInventory().getItemInMainHand().getTypeId() != 0){
					sender.sendMessage(ChatColor.RED + "Vous etes trop loin du spawn");
				}
			}
		}else if (cmd.getName().equalsIgnoreCase("money")){
			if (config.get("players." + sender.getName()) != null){
				sender.sendMessage(ChatColor.AQUA + "Vous possedez " + config.get("players." + sender.getName()) + " Coins");
			}else{
				sender.sendMessage(ChatColor.AQUA + "Vous possedez 0 Coins");
			}
		}
		return true;
	}
	
	@Override
    public void onDisable(){
		try {
			config.save(new File("plugins/BTFS/database.yml"));
		} catch (IOException e) {
			Bukkit.getConsoleSender().sendMessage(e.getMessage());
		}
    }
	
	private void updateWeb(double pr){
		String out2 = "<div id=\"litm\"><div id=\"nr\">TOP 3</div>\n";
		String[] plys = new String[3];
		int[] vals = new int[3];
		vals[0] = 0;
		vals[1] = 0;
		vals[2] = 0;
		for (String s : config.getStringList("pList")){
			if (config.getInt("players." + s) >= vals[0]){
				vals[0] = config.getInt("players." + s);
				plys[0] = s;
			}else if (config.getInt("players." + s) >= vals[1]){
				vals[1] = config.getInt("players." + s);
				plys[1] = s;
			}else if (config.getInt("players." + s) >= vals[2]){
				vals[2] = config.getInt("players." + s);
				plys[2] = s;
			}
		}
		for (int i=0; i < 3; i++){
			if (plys[i] != null){
				out2 += "<div id=\"player\"><div id=\"pName\">" + plys[i] + "</div><div id=\"pMoney\">" + vals[i] + "</div></div>\n";
			}else{
				out2 += "<div id=\"player\"><div id=\"pName\">-----</div><div id=\"pMoney\">-----</div></div>\n";
			}
		}
		String out = out2 + "</div><div id=\"litm\"><div id=\"nr\">Le prix d'un Item non repertorie est : " + pr + " Coins</div>\n";
		int size = 0;
		for (String o : config.getStringList("list")){
			if ((config.getInt("items." + o + ".id") >= 345 && (config.getInt("items." + o + ".id") <= 347)) || (config.getInt("items." + o + ".id") >= 256 && (config.getInt("items." + o + ".id") <= 317)) || config.getInt("items." + o + ".id") == 359){
				out += "<div id=\"item\"><img id=\"icon\" src=\"imgs/" + config.getInt("items." + o + ".id") + "-0.png\"></img><div id=\"" + size + "\" class=\"itemName\">" + config.getInt("items." + o + ".id") + "-" + config.getInt("items." + o + ".dammage") + "</div><div id=\"price\">" + config.get("items." + o + ".price") + " Coins</div></div>\n";
			}else{
				out += "<div id=\"item\"><img id=\"icon\" src=\"imgs/" + config.getInt("items." + o + ".id") + "-" + config.getInt("items." + o + ".dammage") + ".png\"></img><div id=\"" + size + "\" class=\"itemName\">" + config.getInt("items." + o + ".id") + "-" + config.getInt("items." + o + ".dammage") + "</div><div id=\"price\">" + config.get("items." + o + ".price") + " Coins</div></div>\n";
			}
			size++;
		}
		out += "<span id=\"size\">" + size + "</span></div>";
		File file = new File(configB.getString("webFloder"));
		if (!file.exists()){
			try{
				file.createNewFile();
			}catch(IOException e){
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.toString());
			}
		}
		try{
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(fw);
			out = form.replace("<insert>", out);
			pw.println(out);
			pw.flush();
			pw.close();
		}catch (IOException e){
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.toString());
		}	
		
	}
	
	private void addPlayerToList(String name){
		List<String> lst = config.getStringList("pList");
		lst.add(name);
		config.set("pList", lst);
	}
	
}
