package fr.skydunet.xfight;

import org.bukkit.entity.Player;

public class PluginTeam {
	
	private String teamName;
	private int playersCount;
	private Player[] players;
	public String[] playersByName;
	private String color;
	
	public PluginTeam(String name){
		this.teamName = name;
		this.playersCount = 0;
		this.players = new Player[0];
		this.playersByName = new String[0];
	}
	
	public String getName(){
		return this.teamName;
	}
	
	public void removePlayers(){
		this.players = new Player[0];
		this.playersByName = new String[0];
	}
	
	public void addPlayer(Player player){
		Player[] tmpPl = this.players.clone();
		this.players = new Player[tmpPl.length + 1];
		int i = 0;
		for (Player tmp : tmpPl){
			this.players[i] = tmp;
			i++;
		}
		this.players[i] = player;
	}

	public boolean isFull(int plCount, int tmCount) {
		int tmpPl = plCount / tmCount ;
		if (tmpPl >= this.playersCount){
			return false;
		}else{
			return true;
		}
	}
	
	public void addPlayerByName(String name){
		String[] tmpPl = this.playersByName.clone();
		this.playersByName = new String[tmpPl.length + 1];
		int i = 0;
		for (String tmp : tmpPl){
			this.playersByName[i] = tmp;
			i++;
		}
		this.playersByName[i] = name;
	}

	public String[] playersByName() {
		return this.playersByName;
	}
	
	public Player[] getPlayers(){
		return this.players;
	}
	
	public void setColor(String tmpColor){
		this.color = tmpColor;
	}
	
	public String getColor(){
		return this.color;
	}
}
