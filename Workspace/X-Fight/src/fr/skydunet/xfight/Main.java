package fr.skydunet.xfight;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	
	private Timer timer;
	private String scName;
	private Teams teams = new Teams();
	private boolean inited = false;
	
	@Override
    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, this);
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("xfight")){//faire la commande de help
			if (args.length == 1){
				if (args[0].equalsIgnoreCase("start")){
					if (inited == false){
						teams.start();
					}
					if (scName == null){
						timer = new Timer("X-Fight");
					}else{
						timer = new Timer(scName);
					}
					timer.start();
					if (inited == false){
						update();
						inited = true;
					}
					Bukkit.broadcastMessage(ChatColor.AQUA + "D�marage du jeu ! Bonne Chance !");
					return true;
				}else if (args[0].equalsIgnoreCase("stop")){
					timer.stop();
					return true;
				}else{
					return false;
				}
			}else if (args.length == 2){
				if (args[0].equalsIgnoreCase("name")){
					if (args[1] != null){
						scName = args[1];
						return true;
					}
				}else if (args[0].equalsIgnoreCase("random")){
					if (args[1].equalsIgnoreCase("true")){
						teams.randomState(true);
					}else if (args[1].equalsIgnoreCase("false")){
						teams.randomState(false);
					}
					return true;
				}
				if (args[0].equalsIgnoreCase("border")){
					if (args[1] != null){
						teams.border = Integer.parseInt(args[1]);
					}
				}
			}else if (args.length == 3){
				if (args[0].equalsIgnoreCase("name")){
					if (args[1] != null){
						scName = args[1] + " " + args[2];
					}else if (args[0].equalsIgnoreCase("teams")){
						if (args[1].equalsIgnoreCase("add")){
							teams.newTeam(args[2]);
						}else if (args[1].equalsIgnoreCase("true")){
							teams.teamsState = true;
						}else if (args[1].equalsIgnoreCase("false")){
							teams.teamsState = false;
						}
					}
					return true;
				}
			}else if (args.length == 4){
				if (args[0].equalsIgnoreCase("teams") && args[1].equalsIgnoreCase("join")){
					teams.addPlayer(args[2], args[3]);
					return true;
				}
				if (args[0].equalsIgnoreCase("teams") && args[1].equalsIgnoreCase("color")){
					if (teams.setColor(args[2], args[3]) == false){
						sender.sendMessage(ChatColor.RED + "Le nom de la Team ou la couleur est invalide!");
					}
					return true;
				}
			}else{
				return false;
			}
		}
		return false;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		timer.display(player);
		teams.display(player);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		if (timer.gameState == true){
			Player player = e.getEntity().getPlayer();
			player.setGameMode(GameMode.SPECTATOR);
			timer.playerDie(player);
		}
	}
	
	public void update(){
		Bukkit.getScheduler().runTaskTimer(this, new Runnable(){
			@Override
			public void run() {
				timer.update();
			}
		}, 0, 20);
	}
	
	@Override
    public void onDisable(){
        
    }
}
