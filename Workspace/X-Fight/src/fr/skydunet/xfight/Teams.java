package fr.skydunet.xfight;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.WorldBorder;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class Teams {
	
	private boolean random = false;
	public boolean teamsState = false;
	private CommandSender cmdSender = Bukkit.getConsoleSender();
	private PluginTeam[] plTeam = new PluginTeam[0];
	private ScoreboardManager scManager = Bukkit.getScoreboardManager();
	private Scoreboard board = scManager.getNewScoreboard();
	private Scoreboard scoreboard = scManager.getNewScoreboard();
	private Objective obj;
	private Team[] scTeams = new Team[0];
	public int border = 1000;
	private WorldBorder wb = Bukkit.getWorld("world").getWorldBorder();
	
	public void randomState(boolean state){
		this.random = state;
	}
	
	public PluginTeam getTeamObject(String name){
		PluginTeam result = null;
		for (PluginTeam tmpTeam : plTeam){
			if (tmpTeam.getName() == name){
				result = tmpTeam;
			}
		}
		return result;
	}
	
	public void start(){
		new Players().setTeams(teamsState);
		if (teamsState == false){
			for (Player player : Bukkit.getOnlinePlayers()){
				String name = player.getName();
				PluginTeam[] tmpList = plTeam.clone();
				plTeam = null;
				plTeam = new PluginTeam[tmpList.length + 1];
				int i = 0;
				for (PluginTeam tmpTeam : tmpList){
					plTeam[i] = tmpTeam;
					i++;
				}
				plTeam[i] = new PluginTeam(name);
				plTeam[i].addPlayer(player);
				setColor(name, "white");
			}
		}
		for (PluginTeam tmpTeam : plTeam){
			if (tmpTeam.playersByName().length != 0){
				for (String name : tmpTeam.playersByName()){
					boolean found = false;
					for (Player player : Bukkit.getOnlinePlayers()){
						if (player.getName() == name){
							found = true;
							tmpTeam.addPlayer(player);
						}
					}
					if (found == false){
						Bukkit.broadcastMessage(ChatColor.RED + name + " n'a pas �t� trouv� dans les joueurs connect� !");
					}
				}
			}
		}
		if (this.random == true && teamsState == true){
			for (PluginTeam tmpTeam : plTeam){
				tmpTeam.removePlayers();
			}
			for (Player tmpPlayer : Bukkit.getOnlinePlayers()){
				int num = random();
				plTeam[num].addPlayer(tmpPlayer);
			}
		}
		for (PluginTeam tmpTeam : plTeam){
			Team[] tmpList = scTeams.clone();
			scTeams = new Team[tmpList.length + 1];
			int i = 0;
			for (Team tmpScTeam : tmpList){
				scTeams[i] = tmpScTeam;
				i++;
			}
			scTeams[i] = board.registerNewTeam(tmpTeam.getName());
			scTeams[i].setPrefix(tmpTeam.getColor());
			for (Player player : tmpTeam.getPlayers()){
				scTeams[i].addPlayer(player);
			}
		}
		obj = scoreboard.registerNewObjective("vie", "health");
		obj.setDisplayName("Vie");
		obj.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		for (Player player : Bukkit.getOnlinePlayers()){
			player.setScoreboard(scoreboard);
		}
		Bukkit.dispatchCommand(cmdSender, "spreadplayers 0 0 20 " + border + " true @a");
		wb.setCenter(0, 0);
		wb.setSize(border * 2);
		Bukkit.dispatchCommand(cmdSender, "gamerule naturalRegeneration false");
		Bukkit.dispatchCommand(cmdSender, "time set 0");
	}
	
	private int random() {
		int num = (int) (Math.random() * plTeam.length);
		while (plTeam[num].isFull(Bukkit.getOnlinePlayers().size(), plTeam.length)){
			num = (int) (Math.random() * plTeam.length);
		}
		return num;
	}

	public void newTeam(String name){
		PluginTeam[] tmpList = plTeam.clone();
		plTeam = null;
		plTeam = new PluginTeam[tmpList.length + 1];
		int i = 0;
		for (PluginTeam tmpTeam : tmpList){
			plTeam[i] = tmpTeam;
			i++;
		}
		plTeam[i] = new PluginTeam(name);
	}

	public boolean addPlayer(String team, String playerName) {
		boolean found = false;
		for (PluginTeam plTm : plTeam){
			if (plTm.getName() == team){
				found = true;
				plTm.addPlayerByName(playerName);
			}
		}
		return found;
	}
	
	public boolean setColor(String team, String color){
		boolean found = false;
		for (PluginTeam plTm : plTeam){
			if (plTm.getName() == team){
				found = true;
				plTm.setColor(updateColor(color));
			}
		}
		return found;
	}
	
	private String updateColor(String color){
		String result = ChatColor.WHITE + "";
		if (color == "aqua"){
			result = ChatColor.WHITE + "";
		}
		if (color == "black"){
			result = ChatColor.WHITE + "";
		}
		if (color == "blue"){
			result = ChatColor.WHITE + "";
		}
		if (color == "gray"){
			result = ChatColor.WHITE + "";
		}
		if (color == "green"){
			result = ChatColor.WHITE + "";
		}
		if (color == "lime"){
			result = ChatColor.WHITE + "";
		}
		if (color == "maroon"){
			result = ChatColor.WHITE + "";
		}
		if (color == "orange"){
			result = ChatColor.WHITE + "";
		}
		if (color == "purple"){
			result = ChatColor.WHITE + "";
		}
		if (color == "red"){
			result = ChatColor.WHITE + "";
		}
		if (color == "silver"){
			result = ChatColor.WHITE + "";
		}
		if (color == "yellow"){
			result = ChatColor.WHITE + "";
		}
		return result;
	}
	
	public void display(Player player){
		player.setScoreboard(scoreboard);
	}
}
