package fr.skydunet.xfight;

import java.util.Collection;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Players {
	
	private int playersCount;
	private Player[] playersList;
	private boolean teams = false;
	
	public Players(){
		playersCount = Bukkit.getOnlinePlayers().size();
		Collection<? extends Player> playersListTmp = Bukkit.getOnlinePlayers();
		playersList = new Player[playersCount];
		int k = 0;
		for (Player plTmp : playersListTmp){
			playersList[k] = plTmp;
			k++;
		}
	}
	
	public int getPlayersCount(){
		int result = playersCount;
		if (teams == true){
			//calculer le nombre de teams restantes
		}
		return result;
	}
	
	public int getTeamsCount(){
		return playersCount;//ajouter le cas ou les teams sont activ�es
	}
	
	public boolean getPlayer(Player player){
		for (Player list : playersList){
			if (list == player){
				return true;
			}
		}
		return false;
	}
	
	public boolean playerDie(Player player){
		int g = 0;
		for (Player plTmp : playersList){
			if (plTmp == player){
				playersList[g] = null;
			}
			g++;
		}
		playersCount--;
		return true;
	}
	
	public void setTeams(boolean state){
		teams = state;
	}
}
