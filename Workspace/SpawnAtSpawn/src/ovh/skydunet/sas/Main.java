package ovh.skydunet.sas;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	
	public void onEnable(){//charger la config
        Bukkit.getPluginManager().registerEvents(this, this);
	}
	
	public void onDisable(){
		
	}
	
	@EventHandler
	private void onPlayerJoin(PlayerJoinEvent e){
		e.getPlayer().teleport(new Location(e.getPlayer().getWorld(), 0, 200, 0));
	}
}
