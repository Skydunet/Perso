package ovh.skydunet.localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Main extends JavaPlugin implements Listener{
	
	private Timer timer;//ajouter la sauvegarde de la config quand on �teint le serv
	private boolean inited = false;
	private String scName = "Localization";//Cube surement a remplacer mais je sais pas encore par quoi ????
	private String borderSize = "2000";
	private List<TeamBuffer> teamsList = new ArrayList<TeamBuffer>();
	private WorldBorder wBorder = Bukkit.getWorlds().get(0).getWorldBorder();
	private String plName = ChatColor.AQUA + "[" + ChatColor.GOLD + "Localization" + ChatColor.AQUA + "] " + ChatColor.RESET;
	private boolean displayCoordsMsg = false;
	private List<Player> notInTeam = new ArrayList<Player>();
	private int colorState = 0;
	private ShapedRecipe goldenMelon;
	
	@Override
    public void onEnable(){//charger la config
        Bukkit.getPluginManager().registerEvents(this, this);
        for (World w : Bukkit.getServer().getWorlds()){
        	w.setGameRuleValue("naturalRegeneration", "false");
        }
        goldenMelon = new ShapedRecipe(new ItemStack(Material.SPECKLED_MELON, 1));
        goldenMelon.shape("***", "*-*", "***");
        goldenMelon.setIngredient('*', Material.GOLD_INGOT);
        goldenMelon.setIngredient('-', Material.MELON);
        Bukkit.getServer().addRecipe(goldenMelon);
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){//ajouter un /join qui n'est pas pour les ops
		if (cmd.getName().equalsIgnoreCase("uhc")){//faire la commande de help
			if (args.length == 1){
				if (args[0].equalsIgnoreCase("start")){
					if (!inited){
						if (scName == null){
							timer = new Timer("UHC");
						}else{
							timer = new Timer(scName);
						}
						//initialiseTeams();//on le met pas car c'est du FFA au debut
						timer.getPlayersClass().setTeams(true);
						for (TeamBuffer t : teamsList){
							t.generate(timer.getPlayersClass(), timer.getScoreboard());
						}
						for (Player p : Bukkit.getOnlinePlayers()){
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 600, 255, true));
							notInTeam.add(p);
							p.getInventory().clear();
							p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20, 255, true));
							p.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 20, 255, true));
						}
						spreadTeams();//spread les players
						wBorder.setCenter(0, 0);
						wBorder.setSize(Integer.parseInt(borderSize));
					}
					timer.start();
					if (inited == false){
						update();
						inited = true;
					}
					Bukkit.broadcastMessage(ChatColor.AQUA + "D�marage du jeu ! Bonne Chance !");
					return true;
				}else if (args[0].equalsIgnoreCase("stop")){
					if (inited){
						timer.stop();
					}
					return true;
				}else if (args[0].equalsIgnoreCase("bypass")){
					if (inited){
						wBorder.setSize(200, 600);//ajouter le display de la taille quand ca reduit
						for (Player p : Bukkit.getOnlinePlayers()){
							p.sendMessage(ChatColor.RED + sender.getName() + " vient d'activer la r�dution de la bordure");
						}
					}
					return true;
				}else if (args[0].equalsIgnoreCase("addepi")){
					timer.epi++;
				}else{
					return false;
				}
			}else if (args.length == 2){
				if (args[0].equalsIgnoreCase("name")){
					if (args[1] != null){
						scName = args[1];
						return true;
					}
				}
				if (args[0].equalsIgnoreCase("border")){
					if (args[1] != null){
						borderSize = args[1];
						return true;
					}
				}
			}else if (args.length == 3){
				if (args[0].equalsIgnoreCase("name")){
					if (args[1] != null){
						scName = args[1] + " " + args[2];
					}
					return true;
				}
			}else{
				return false;
			}
		}
		return false;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		timer.display(player);
		if (inited && !timer.getPlayersClass().players.contains(player)){
			player.kickPlayer(ChatColor.RED + "Vous ne faites pas partit de cette partie !");
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		if (timer.gameState == true){
			Player player = e.getEntity().getPlayer();
			player.setGameMode(GameMode.SPECTATOR);
			timer.playerDie(player);
		    Collection<? extends Player> ps = Bukkit.getServer().getOnlinePlayers();
		    for (Player pp : ps) {
		    	pp.playSound(pp.getLocation(), Sound.WITHER_SPAWN, 1.0F, 1.0F);
		    }
		}
	}
	
	public void update(){
		Bukkit.getScheduler().runTaskTimer(this, new Runnable(){
			@Override
			public void run() {
				timer.update();
				if (!displayCoordsMsg){
					if (timer.getSec() >= 59){
						displayCoordsMsg = true;
					}
				}
				if (displayCoordsMsg){
					updateTeams();
				}
			}
		}, 0, 20);
	}
	
	@Override
    public void onDisable(){
        //sauvegarder la config dans un fichier
    }
	
	/*private void initialiseTeams(){//cr�e des teams pour les joueurs sans team 
		for (Player p : Bukkit.getOnlinePlayers()){//si le joeuur n'est as dans une TeamBuffer on lui cr�e une team
			boolean st = false;
			for (TeamBuffer tb : teamsList){
				for (String s : tb.players){
					if (s == p.getName()){
						st = true;
					}
				}
			}
			if (!st){
				Teams t = timer.getPlayersClass().addTeam(p.getName(), ChatColor.RESET, timer.getScoreboard());
				t.addPlayer(p);
			}
		}
	}*/
	
	private void spreadTeams(){//enf ait la c'est les players mais la flemme de tout rennomer
		for (Player p : Bukkit.getOnlinePlayers()){
			Random rnd = new Random();
			int x = rnd.nextInt(Integer.parseInt(borderSize)) - (Integer.parseInt(borderSize) / 2);
			int z = rnd.nextInt(Integer.parseInt(borderSize)) - (Integer.parseInt(borderSize) / 2);
			World w = Bukkit.getWorlds().get(0);
			while (w.getBlockAt(x, w.getHighestBlockYAt(x, z)-1, z).getType() == Material.WATER || w.getBlockAt(x, w.getHighestBlockYAt(x, z)-1, z).getType() == Material.STATIONARY_WATER){
				x = rnd.nextInt(Integer.parseInt(borderSize)) - (Integer.parseInt(borderSize) / 2);
				z = rnd.nextInt(Integer.parseInt(borderSize)) - (Integer.parseInt(borderSize) / 2);
				Bukkit.getConsoleSender().sendMessage("eh ba on scan pcq on est sur de l'eau xD");
			}
			p.teleport(new Location(p.getWorld(), x, 255, z));
		}
	}
	
	@EventHandler
	private void playerAchivement(PlayerAchievementAwardedEvent e){
		if (inited){
			if (displayCoordsMsg){
				displayPlayerCoords(e.getPlayer());
			}
			for (Player p : Bukkit.getOnlinePlayers()){
				String pName = "";
				if (notInTeam.contains(p)){
				pName = "?";
				}else{
					pName = e.getPlayer().getName();
				}
				String an = convertAchievement(e.getAchievement());
				if (an != null){
					p.sendMessage(plName + ChatColor.GREEN + pName + " has just earned the achievement " + an);
				}else{
					p.sendMessage(plName + ChatColor.GREEN + pName + " has just earned an achievement");
				}
			}
		}else{
			e.setCancelled(true);
		}
	}
	
	private void displayPlayerCoords(Player p){
		for (Player pp : Bukkit.getOnlinePlayers()){
			boolean inTeam = false;
			for (Teams t : timer.getPlayersClass().teamsList){
				for (Player ppp : t.players){
					if (pp == ppp){
						inTeam = true;
					}
				}
			}
			if (inTeam){
				pp.sendMessage(plName + ChatColor.GREEN + "Le joueur " + p.getName() + " se trouve en " + p.getLocation().getBlockX() + " " + p.getLocation().getBlockY() + " " + p.getLocation().getBlockZ());
			}else{
				pp.sendMessage(plName + ChatColor.GREEN + "Un joueur se trouve en " + p.getLocation().getBlockX() + " " + p.getLocation().getBlockY() + " " + p.getLocation().getBlockZ());
			}
		}
	}
	
	@EventHandler
	private void playerTakeDammage(EntityDamageEvent e){
		if (e.getEntity() instanceof Player){
			Random rnd = new Random();
			int out = rnd.nextInt(2);
			if (out == 0 && displayCoordsMsg){
				displayPlayerCoords((Player)e.getEntity());
			}
		}
	}
	
	@EventHandler
	private void playerTakeDamageStrenght(EntityDamageByEntityEvent e){
		if (e.getDamager() instanceof Player){
			Player p = (Player)e.getDamager();
			if (p.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)){
				for (PotionEffect eff : p.getActivePotionEffects()){
					if (eff.getType() == PotionEffectType.INCREASE_DAMAGE){
						e.setDamage(e.getDamage() / (((eff.getAmplifier() + 1) * 1.3) + 1) / 2);
						break;
					}
				}
			}
		}
	}
	
	private void updateTeams(){
		for (Player p : notInTeam){
			if (notInTeam.contains(p)){
				for (Player pp : notInTeam){
					if (p != pp){
						if (p.getLocation().distance(pp.getLocation()) <= 50){
							notInTeam.remove(p);
							notInTeam.remove(pp);
							String out = Integer.toHexString(colorState);
							colorState++;
							ChatColor c = ChatColor.getByChar(out);
							if (colorState == 16){
								colorState = 0;
							}
							Teams t = timer.getPlayersClass().addTeam(p.getName(), c, timer.getScoreboard());
							t.addPlayer(p);
							t.addPlayer(pp);
							for (Player ppp : Bukkit.getOnlinePlayers()){
								ppp.sendMessage(plName + ChatColor.GREEN + p.getName() + " et " + pp.getName() + " font d�sormais parti de la m�me �quipe !");
							}
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	private void checkRightClick(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if (p.getInventory().getItemInHand().getType() == Material.COMPASS && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)){
			if (timer.epi >= 5){
				if (p.getInventory().contains(Material.IRON_INGOT)){
					//p.getInventory().removeItem(new ItemStack(Material.IRON_INGOT, 1));
					removeItem(p, new ItemStack(Material.IRON_INGOT, 1));
					checkNearestPlayer(p);
				}else{
					p.sendMessage(plName + ChatColor.RED + "Vous n'avez pas d'Or dans votre inventaire !");
				}
			}else if (timer.epi >= 2){
				if (p.getInventory().contains(Material.GOLD_INGOT)){
					//p.getInventory().removeItem(new ItemStack(Material.GOLD_INGOT, 1));
					removeItem(p, new ItemStack(Material.GOLD_INGOT, 1));
					checkNearestPlayer(p);
				}else{
					p.sendMessage(plName + ChatColor.RED + "Vous n'avez pas d'Or dans votre inventaire !");
				}
			}else{
				p.sendMessage(plName + ChatColor.RED + "Vous ne pouvez utiliser la bousole qu'a partir de l'�pisode 2 !");
			}
		}
		if (p.getInventory().getItemInHand().getType() == Material.POTION && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)){
			Potion po = Potion.fromItemStack(p.getInventory().getItemInHand());
			if (po.getLevel() >= 2){
				e.getPlayer().sendMessage(plName + ChatColor.RED + "Les Potions de Niveau II sont interdites !");
				e.setCancelled(true);
				p.updateInventory();
			}
		}
	}
	
	private void checkNearestPlayer(Player p){
		double dist = 0;
		Player nst = null;
		boolean plFound = false;
		Player tm = null;
		if (!notInTeam.contains(p)){
			for (Teams t : timer.getPlayersClass().teamsList){
				if (t.players.contains(p)){
					for (Player pp : t.players){
						if (p != pp){
							tm = pp;
						}
					}
				}
			}
		}
		for (Player pp : Bukkit.getOnlinePlayers()){
			if (p.getWorld() == pp.getWorld() && p != pp && pp != tm && timer.getPlayersClass().getPlayer(pp)){
				plFound = true;
				if (p.getLocation().distance(pp.getLocation()) <= dist){
					dist = p.getLocation().distance(pp.getLocation());
					nst = pp;
				}else if (dist == 0){
					dist = p.getLocation().distance(pp.getLocation());
					nst = pp;
				}
			}
		}
		if (plFound){
			p.setCompassTarget(nst.getLocation());
			p.sendMessage(plName + ChatColor.GREEN + "Votre bousole pointe maintenant vers le joueur le plus proche de vous !");
		}else{
			p.sendMessage(plName + ChatColor.RED + "Aucun joueur n'a �t� trouv� dans votre monde");
		}
	}
	
	@SuppressWarnings("deprecation")
	private void removeItem(Player p, ItemStack item){
		p.getInventory().removeItem(item);
		p.updateInventory();
	}
	
	private String convertAchievement(Achievement a){
		if (a == Achievement.GET_DIAMONDS){
			return ChatColor.AQUA + "[DIAMONDS!]";
		}
		if (a == Achievement.DIAMONDS_TO_YOU){
			return ChatColor.AQUA + "[Diamonds To You]";
		}
		if (a == Achievement.ENCHANTMENTS){
			return ChatColor.AQUA + "[Enchanter]";
		}
		if (a == Achievement.NETHER_PORTAL){
			return ChatColor.DARK_PURPLE + "[We Need to Go Deeper]";
		}
		if (a == Achievement.GHAST_RETURN){
			return ChatColor.AQUA + "[Return to Sender]";
		}
		if (a == Achievement.GET_BLAZE_ROD){
			return ChatColor.AQUA + "[Into Fire]";
		}
		if (a == Achievement.OVERKILL){
			return ChatColor.DARK_PURPLE + "[Overkill]";
		}
		if (a == Achievement.BREW_POTION){
			return ChatColor.AQUA + "[Local Brewery]";
		}
		if (a == Achievement.BUILD_HOE){
			return "[Time to Farm !]";
		}
		return null;
	}
	
	@EventHandler
	private void prepareCraft(PrepareItemCraftEvent e){
        if (e.getInventory().getItem(0).getType() == Material.SPECKLED_MELON && e.getInventory().getItem(2).getType() == Material.GOLD_NUGGET){
        	e.getInventory().setResult(new ItemStack(Material.AIR, 1));
        	e.getView().getPlayer().sendMessage(plName + ChatColor.RED + "Vous devez utiliser des lingots d'Or !");
        }
	}
	
	@EventHandler
	private void onChatMessage(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		if (e.getMessage().substring(0, 1) == "!"){
			if (!notInTeam.contains(p)){
				boolean st = false;
				Teams tt = null;
				for (Teams t : timer.getPlayersClass().teamsList){
					if (t.players.contains(p)){
						st = true;
						tt = t;
					}
				}
				if (st){
					if (!tt.playersDead.contains(p)){
						for (Player pp : tt.players){
							pp.sendMessage(ChatColor.GOLD + "TEAM : " + p.getName() + " : " + ChatColor.RESET + e.getMessage().substring(1, e.getMessage().length()-1));
						}
					}else{
						p.sendMessage(plName + ChatColor.RED + "Vous ne pouvez pas envoyer de message d'�quipe lorsque vous etes mort");
					}
				}
			}else{
				p.sendMessage(plName + ChatColor.RED + "Vous ne faites pas parti d'une equipe");
			}
		}
	}
}
