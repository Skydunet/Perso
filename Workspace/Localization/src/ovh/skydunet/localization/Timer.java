package ovh.skydunet.localization;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class Timer {
	
	private String scName;
	private ScoreboardManager scManager = Bukkit.getScoreboardManager();
	private Scoreboard scoreboard = scManager.getNewScoreboard();
	private Objective obj = scoreboard.registerNewObjective("ktp", "dummy");
	private int pastTime[] = new int[4];
	private boolean inited;
	private Players playersCount = new Players();
	public int epi;
	public boolean gameState = false;
	private String[] previous = new String[4];
	private String[] display = new String[3];
	
	public Timer(String scoreboardName){
		this.scName = scoreboardName;
		this.inited = false;
	}

	public void stop(){
		gameState = false;
		for (Player player : Bukkit.getOnlinePlayers()){
			player.setGameMode(GameMode.ADVENTURE);
		}
	}
	
	public void start(){
		if (this.inited == false){
			init();
		}
		gameState = true;
		for (Player player : Bukkit.getOnlinePlayers()){
			player.setGameMode(GameMode.SURVIVAL);
		}
	}
	
	private void init(){
		this.inited = true;
		obj.setDisplayName(this.scName);
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		for (Player online : Bukkit.getOnlinePlayers()){
			online.setScoreboard(scoreboard);
		}
		pastTime[0] = 0;
		pastTime[1] = 0;
		pastTime[2] = 0;
		pastTime[3] = 1;
		display[0] = "00";
		display[1] = "00";
		display[2] = "00";
		previous[3] = "1";
		Score time = obj.getScore("00:00:00");
		time.setScore(-5);
		Score playersDisplay = obj.getScore(ChatColor.GRAY + "Players " + ChatColor.WHITE + playersCount.getPlayersCount());
		playersDisplay.setScore(-3);
		Score space = obj.getScore(" ");
		space.setScore(-4);
		epi = 1;
		Score episode = obj.getScore(ChatColor.GRAY + "Episode " + ChatColor.WHITE + epi);
		episode.setScore(-1);
		Score teams = obj.getScore(ChatColor.GRAY + "Teams " + ChatColor.WHITE + playersCount.getTeamsCount());
		teams.setScore(-2);
	}
	
	public void display(Player player){
		player.setScoreboard(scoreboard);
	}
	
	public void playerDie(Player player){
		playersCount.playerDie(player);
		obj.getScoreboard().resetScores(ChatColor.GRAY + "Players " + ChatColor.WHITE + (playersCount.getPlayersCount() + 1));
		obj.getScoreboard().resetScores(ChatColor.GRAY + "Teams " + ChatColor.WHITE + (playersCount.getTeamsCount() + 1));
		Score playersDisplay = obj.getScore(ChatColor.GRAY + "Players " + ChatColor.WHITE + playersCount.getPlayersCount());
		playersDisplay.setScore(-3);
		Score teams = obj.getScore(ChatColor.GRAY + "Teams " + ChatColor.WHITE + playersCount.getTeamsCount());
		teams.setScore(-2);
		display(player);
	}
	
	public void update(){
		if (gameState){
			previous[0] = "" + display[0];
			previous[1] = "" + display[1];
			previous[2] = "" + display[2];
			pastTime[0]++;
			if (pastTime[0] == 60){
				pastTime[0] = 0;
				pastTime[1]++;
			}
			if (pastTime[1] == 60 && pastTime[0] == 0){
				pastTime[1] = 0;
				pastTime[2]++;
				epi++;
				Bukkit.broadcastMessage(ChatColor.AQUA + "--- Fin de l'�pisode " + (epi - 1) + " ---");
				obj.getScoreboard().resetScores(ChatColor.GRAY + "Episode " + ChatColor.WHITE + (epi - 1));
				Score epit = obj.getScore(ChatColor.GRAY + "Episode " + ChatColor.WHITE + epi);
				epit.setScore(-1);
			}
			if (pastTime[1] == 40 && pastTime[0] == 0){
				epi++;
				Bukkit.broadcastMessage(ChatColor.AQUA + "--- Fin de l'�pisode " + (epi - 1) + " ---");
				obj.getScoreboard().resetScores(ChatColor.GRAY + "Episode " + ChatColor.WHITE + (epi - 1));
				Score epit = obj.getScore(ChatColor.GRAY + "Episode " + ChatColor.WHITE + epi);
				epit.setScore(-1);
			}
			if (pastTime[1] == 20 && pastTime[0] == 0){
				epi++;
				Bukkit.broadcastMessage(ChatColor.AQUA + "--- Fin de l'�pisode " + (epi - 1) + " ---");
				obj.getScoreboard().resetScores(ChatColor.GRAY + "Episode " + ChatColor.WHITE + (epi - 1));
				Score epit = obj.getScore(ChatColor.GRAY + "Episode " + ChatColor.WHITE + epi);
				epit.setScore(-1);
			}
			display[0] = "" + pastTime[0];
			display[1] = "" + pastTime[1];
			display[2] = "" + pastTime[2];
			if (display[0].length() != 2){
				display[0] = "0" + display[0];
			}
			if (display[1].length() != 2){
				display[1] = "0" + display[1];
			}
			if (display[2].length() != 2){
				display[2] = "0" + display[2];
			}
			obj.getScoreboard().resetScores(previous[2] + ":" + previous[1] + ":" + previous[0]);
			Score playersDisplay = obj.getScore(display[2] + ":" + display[1] + ":" + display[0]);
			playersDisplay.setScore(-5);
		}
	}
	
	public Players getPlayersClass(){
		return playersCount;
	}
	
	public Scoreboard getScoreboard(){
		return scoreboard;
	}
	
	public int getSec(){
		return pastTime[0];
	}
	
}
