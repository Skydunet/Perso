package ovh.skydunet.localization;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class Teams {
	
	private String name;
	private ChatColor color;
	private Team team;
	public List<Player> players = new ArrayList<Player>();
	public List<Player> playersDead = new ArrayList<Player>();
	
	
	public Teams(String name, ChatColor color, Scoreboard sc){
		this.name = name;
		this.color = color;
		
	    sc.registerNewTeam(this.name);
	    
	    Team t = sc.getTeam(this.name);
	    t.setDisplayName(this.name);
	    t.setCanSeeFriendlyInvisibles(true);
	    t.setPrefix(this.color + "");
	    
	    this.team = t;
	}

	public boolean getAlivePlayers(){
		Bukkit.getConsoleSender().sendMessage("on texte");
		boolean out = true;
		for (Player p : this.players){
			Bukkit.getConsoleSender().sendMessage("on texte2");
			if (p.isOnline()){
				Bukkit.getConsoleSender().sendMessage("on texte3");
				boolean sta = true;
				for (Player pld : this.playersDead){
					Bukkit.getConsoleSender().sendMessage("on texte4");
					if (p == pld){
						sta = false;
						Bukkit.getConsoleSender().sendMessage("p" + p.toString());
						Bukkit.getConsoleSender().sendMessage("pld" + pld.toString());
					}
				}
				if (!sta){
					out = false;
				}
			}
		}
		return out;
	}
	
	public void addPlayer(Player p){
		this.team.addPlayer(p);
		this.players.add(p);
	}
	
	public void playerDie(Player p){
		this.playersDead.add(p);
		Bukkit.getConsoleSender().sendMessage("un joueur est mort xD");
	}
	
}
