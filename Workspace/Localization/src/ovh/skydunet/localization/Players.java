package ovh.skydunet.localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

public class Players {
	
	private int playersCount;
	public Player[] playersList;
	private boolean teams = true;
	public List<Teams> teamsList = new ArrayList<Teams>();
	private List<String> teamsName = new ArrayList<String>();
	public Collection<? extends Player> players;
	
	public Players(){
		playersCount = Bukkit.getOnlinePlayers().size();
		Collection<? extends Player> playersListTmp = Bukkit.getOnlinePlayers();
		playersList = new Player[playersCount];
		int k = 0;
		for (Player plTmp : playersListTmp){
			playersList[k] = plTmp;
			k++;
		}
		players = Bukkit.getOnlinePlayers();
	}
	
	public int getPlayersCount(){
		int result = playersCount;
		return result;
	}
	
	public int getTeamsCount(){
		int tmc = playersCount;
		if (teams){
			int teamsLeft = 0;
			for (Teams t : teamsList){
				if (t.getAlivePlayers()){
					teamsLeft++;
				}
			}
			tmc = teamsLeft;
		}
		return tmc;
	}
	
	public boolean getPlayer(Player player){
		for (Player list : playersList){
			if (list == player){
				return true;
			}
		}
		return false;
	}
	
	public Teams addTeam(String name, ChatColor color, Scoreboard sc){
		teamsName.add(name);
		Teams t = new Teams(name, color, sc);
		teamsList.add(t);
		return t;
	}
	
	public boolean playerDie(Player player){
		int g = 0;
		for (Player plTmp : playersList){
			if (plTmp == player){
				playersList[g] = null;
			}
			g++;
		}
		for (Teams t : teamsList){
			for (Player p : t.players){
				if (p == player){
					t.playerDie(p);
				}
			}
		}
		playersCount--;
		return true;
	}
	
	public void setTeams(boolean state){
		teams = state;
	}
}
