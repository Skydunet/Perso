package ovh.skydunet.uhchost;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

public class TeamBuffer {
	
	public TeamBuffer(){this.color = ChatColor.RESET;}
	public String name;
	public List<String> players = new ArrayList<String>();
	public ChatColor color;
	public Teams t;
	
	public void generate(Players playersEditor, Scoreboard sc){
		this.t = playersEditor.addTeam(this.name, this.color, sc);
		for (String s : players){
			boolean state = false;
			for (Player p : Bukkit.getOnlinePlayers()){
				if (p.getName() == s){
					this.t.addPlayer(p);
					state = true;
				}
			}
			if (!state){
				for (Player p : Bukkit.getOnlinePlayers()){
					p.sendMessage(ChatColor.RED + "Impossible de trouver le joueur " + s);
				}
			}
		}
		for (Player p : Bukkit.getOnlinePlayers()){
			boolean state = false;
			for (Teams tm : playersEditor.teamsList){
				for (Player pl : tm.players){
					if (p == pl){
						state = true;
					}
				}
			}
			if (!state){
				int i = 0;
				for (Player pl : playersEditor.playersList){
					if (pl == p){
						playersEditor.playersList[i] = null;
					}
					i++;
				}
			}
		}
	}
	
}
