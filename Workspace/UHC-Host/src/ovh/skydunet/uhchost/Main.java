package ovh.skydunet.uhchost;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.minecraft.server.v1_8_R1.Block;

public class Main extends JavaPlugin implements Listener{
	
	private Timer timer;//ajouter la sauvegarde de la config quand on �teint le serv
	private boolean inited = false;
	private String scName = ChatColor.GOLD + "UHC" + ChatColor.AQUA + "Cube";//Cube surement a remplacer mais je sais pas encore par quoi ????
	private String borderSize = "2000";
	private List<TeamBuffer> teamsList = new ArrayList<TeamBuffer>();
	private WorldBorder wBorder = Bukkit.getWorlds().get(0).getWorldBorder();
	private int to = 1;//nb de joueurs par team
	private int maxPlayers = 50;//l'host set cettte valeur
	
	@Override
    public void onEnable(){//charger la config
        Bukkit.getPluginManager().registerEvents(this, this);
        for (World w : Bukkit.getServer().getWorlds()){
        	w.setGameRuleValue("naturalRegeneration", "false");
        }
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){//ajouter un /join qui n'est pas pour les ops
		if (cmd.getName().equalsIgnoreCase("uhc")){//faire la commande de help
			if (args.length == 1){
				if (args[0].equalsIgnoreCase("start")){
					if (!inited){
						if (scName == null){
							timer = new Timer("KTP XVII");
						}else{
							timer = new Timer(scName);
						}
						initialiseTeams();//pour les joueurs qui n'ont pas hoisis leur team
						timer.getPlayersClass().setTeams(true);
						for (TeamBuffer t : teamsList){
							t.generate(timer.getPlayersClass(), timer.getScoreboard());
						}
						for (Player p : Bukkit.getOnlinePlayers()){
							p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 600, 255, true));
						}
						spreadTeams();//spread les players
						wBorder.setCenter(0, 0);
						wBorder.setSize(Integer.parseInt(borderSize));
					}
					timer.start();
					if (inited == false){
						update();
						inited = true;
					}
					Bukkit.broadcastMessage(ChatColor.AQUA + "D�marage du jeu ! Bonne Chance !");
					return true;
				}else if (args[0].equalsIgnoreCase("stop")){
					if (inited){
						timer.stop();
					}
					return true;
				}else if (args[0].equalsIgnoreCase("bypass")){
					if (inited){
						wBorder.setSize(200, 600);//ajouter le display de la taille quand ca reduit
						for (Player p : Bukkit.getOnlinePlayers()){
							p.sendMessage(ChatColor.RED + sender.getName() + " vient d'activer la r�dution de la bordure");
						}
					}
					return true;
				}else{
					return false;
				}
			}else if (args.length == 2){
				if (args[0].equalsIgnoreCase("name")){
					if (args[1] != null){
						scName = args[1];
						return true;
					}
				}
				if (args[0].equalsIgnoreCase("border")){
					if (args[1] != null){
						borderSize = args[1];
					}
				}
			}else if (args.length == 3){
				if (args[0].equalsIgnoreCase("name")){
					if (args[1] != null){
						scName = args[1] + " " + args[2];
					}
					return true;
				}else if (args[0].equalsIgnoreCase("teams")){
					if (args[1].equalsIgnoreCase("add")){
						boolean sta = true;
						for (TeamBuffer t : teamsList){
							if (t.name == args[2]){
								sta = false;
							}
						}
						if (sta){
							TeamBuffer tm = new TeamBuffer();
							tm.name = args[2];
							tm.color = ChatColor.WHITE;
							teamsList.add(tm);
							sender.sendMessage(ChatColor.AQUA + "Team cr�e avec success");
						}else{
							sender.sendMessage(ChatColor.RED + "Impossible de crer la Team : Nom deja utilis�");
						}
					}else{
						return false;
					}
					return true;
				}
			}else if (args.length == 4){
				if (args[0].equalsIgnoreCase("teams") && args[1].equalsIgnoreCase("join") && to != 1){
					boolean state = false;
					for (TeamBuffer t : teamsList){
						if (t.name == args[2]){
							if (t.players.size() <= to){
								t.players.add(args[3]);
								sender.sendMessage(ChatColor.AQUA + args[3] + " a �t� ajout� a la team " + args[2]);
							}else{
								sender.sendMessage(ChatColor.RED + "Cette Team est deja au complet");
							}
							state = true;
						}
					}
					if (!state){
						sender.sendMessage(ChatColor.RED + "Impossible de trouver cette Team");
					}
					return true;
				}
				if (args[0].equalsIgnoreCase("teams") && args[1].equalsIgnoreCase("color")){
					/*if (teams.setColor(args[2], args[3]) == false){
						sender.sendMessage(ChatColor.RED + "Le nom de la Team ou la couleur est invalide!");
					}*/
					return true;
				}
			}else{
				return false;
			}
		}
		return false;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		timer.display(player);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		if (timer.gameState == true){
			Player player = e.getEntity().getPlayer();
			player.setGameMode(GameMode.SPECTATOR);
			timer.playerDie(player);
		    Collection<? extends Player> ps = Bukkit.getServer().getOnlinePlayers();
		    for (Player pp : ps) {
		    	pp.playSound(pp.getLocation(), Sound.WITHER_SPAWN, 1.0F, 1.0F);
		    }
		}
	}
	
	public void update(){
		Bukkit.getScheduler().runTaskTimer(this, new Runnable(){
			@Override
			public void run() {
				timer.update();
			}
		}, 0, 20);
	}
	
	@Override
    public void onDisable(){
        //sauvegarder la config dans un fichier
    }
	
	private void initialiseTeams(){//cr�e des teams pour les joueurs sans team 
		for (Player p : Bukkit.getOnlinePlayers()){//si le joeuur n'est as dans une TeamBuffer on lui cr�e une team
			boolean st = false;
			for (TeamBuffer tb : teamsList){
				for (String s : tb.players){
					if (s == p.getName()){
						st = true;
					}
				}
			}
			if (!st){
				Teams t = timer.getPlayersClass().addTeam(p.getName(), ChatColor.RESET, timer.getScoreboard());
				t.addPlayer(p);
			}
		}
	}
	
	private void spreadTeams(){
		for (Teams t : timer.getPlayersClass().teamsList){
			Random rnd = new Random();
			int x = rnd.nextInt(Integer.parseInt(borderSize) / 2);
			int z = rnd.nextInt(Integer.parseInt(borderSize) / 2);
			World w = Bukkit.getWorlds().get(0);
			while (w.getBlockAt(x, w.getHighestBlockYAt(x, z)-1, z).getType() == Material.WATER || w.getBlockAt(x, w.getHighestBlockYAt(x, z)-1, z).getType() == Material.STATIONARY_WATER){
				x = rnd.nextInt(Integer.parseInt(borderSize) / 2);
				z = rnd.nextInt(Integer.parseInt(borderSize) / 2);
				Bukkit.getConsoleSender().sendMessage("eh ba on scan pcq on est sur de l'eau xD");
			}
			for (Player p : t.players){
				p.teleport(new Location(p.getWorld(), x, 255, z));
			}
		}
	}
}
