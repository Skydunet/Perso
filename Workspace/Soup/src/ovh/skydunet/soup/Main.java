package ovh.skydunet.soup;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Main extends JavaPlugin implements Listener{
	
	@Override
	public void onEnable(){
		Bukkit.getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable(){
		
	}
	
	@EventHandler
	public void onPlayerRightClick(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if (p.getInventory().getItemInHand().getType() == Material.MUSHROOM_SOUP && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)){
			p.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 2, 255, true));
			p.getInventory().setItemInHand(null);
		}
	}
	
}
