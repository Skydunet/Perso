package com.skydunet.tptools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	
	private FileConfiguration config;
	private FileConfiguration data;
	private ArrayList<PlayerBuffer> playerBuffer = new ArrayList<PlayerBuffer>();
	
	public void onEnable(){//charger la config
        Bukkit.getPluginManager().registerEvents(this, this);
        File fichierData = new File("plugins/TeleportationTools/data.yml");
        File fichierConfig = new File("plugins/TeleportationTools/config.yml");
        config = YamlConfiguration.loadConfiguration(fichierConfig);
        data = YamlConfiguration.loadConfiguration(fichierData);
        
        if (config.get("tpa") == null){
        	config.set("tpa", true);
        }
        
        if (config.get("home") == null){
        	config.set("home", true);
        }
        
        if (config.get("spawn") == null){
        	config.set("spawn", true);
        }
        
        try{
			config.save(fichierConfig);
		}catch (IOException e){
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + e.getMessage());
		}
	}
	
	public void onDisable(){
		try {
			data.save(new File("plugins/TeleportationTools/data.yml"));
		} catch (IOException e) {
			Bukkit.getConsoleSender().sendMessage(e.getMessage());
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("home")){
			if (data.get("home." + sender.getName()) != null){
				tpPlayer(sender, data.getString("home." + sender.getName()));
			}else{
				sender.sendMessage(ChatColor.RED + "Home not defined");
			}
		}
		if (cmd.getName().equalsIgnoreCase("sethome")){
			for (Player p : Bukkit.getOnlinePlayers()){
				if (p.getName() == sender.getName()){
					Location loc = p.getLocation();
					data.set("home." + p.getName(), loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + "," + loc.getWorld().getName());
					p.sendMessage(ChatColor.AQUA + "Home set");
				}
			}
		}
		if (cmd.getName().equalsIgnoreCase("setspawn")){
			for (Player p : Bukkit.getOnlinePlayers()){
				if (p.getName() == sender.getName()){
					Location loc = p.getLocation();
					data.set("spawn", loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + "," + loc.getWorld().getName());
					p.sendMessage(ChatColor.AQUA + "Spawn set");
				}
			}
		}
		if (cmd.getName().equalsIgnoreCase("spawn")){
			if (data.get("spawn") != null){
				tpPlayer(sender, data.getString("spawn"));
			}else{
				sender.sendMessage(ChatColor.RED + "Spawn not defined");
			}
		}
		if (cmd.getName().equalsIgnoreCase("tpa")){
			if (args.length == 1){
				boolean sta = false;
				for (Player p : Bukkit.getOnlinePlayers()){
					if (p.getName() == args[0]){
						for (Player pp : Bukkit.getOnlinePlayers()){
							if (pp.getName() == sender.getName()){
								boolean s = false;
								for (PlayerBuffer pb : playerBuffer){
									if (pb.p == p){
										s = true;
										sta = true;
										pb.setRequest(pp);
										p.sendMessage(ChatColor.GOLD + pp.getName() + " want to be teleported to you ! Type /tpaccept to accept");
									}
								}
								if (!s){
									PlayerBuffer pb = new PlayerBuffer(p);
									pb.setRequest(pp);
									playerBuffer.add(pb);
									p.sendMessage(ChatColor.GOLD + pp.getName() + " want to be teleported to you ! Type /tpaccept to accept");
								}
							}
						}
					}
				}
				if (!sta){
					sender.sendMessage(ChatColor.RED + "Player not found");
				}
			}else{
				sender.sendMessage(ChatColor.RED + "/tpa <Player>");
			}
		}
		if (cmd.getName().equalsIgnoreCase("tpaccept")){
			boolean s = false;
			for (PlayerBuffer pb : playerBuffer){
				if (pb.p.getName() == sender.getName()){
					s = true;
					boolean st = false;
					for (Player p : Bukkit.getOnlinePlayers()){
						if (p == pb.request){
							st = true;
							p.sendMessage(ChatColor.AQUA + sender.getName() + " accepted your teleportation request ! You've been teleported");
							p.teleport(pb.p);
						}
					}
					if (!st){
						sender.sendMessage(ChatColor.RED + "The player is no longer connected");
					}
				}
			}
			if (!s){
				sender.sendMessage(ChatColor.RED + "You don't have teleportation request");
			}
		}
		return true;
	}
	
	private void tpPlayer(CommandSender sender, String coord){
		String[] SCoords = coord.split(",");
		double[] coords = new double[3];
		for (int i = 0; i < 3; i++){
			coords[i] = Integer.parseInt(SCoords[i]);
		}
		World w = Bukkit.getWorld(SCoords[3]);
		if (w != null){
			for (Player p : Bukkit.getOnlinePlayers()){
				if (p.getName() == sender.getName()){
					sender.sendMessage(ChatColor.AQUA + "You've been teleported");
					p.teleport(new Location(w, coords[0] + 0.5, coords[1], coords[2] + 0.5));
				}
			}
		}else{
			sender.sendMessage(ChatColor.RED + "Location not found");
		}
	}
	
}
